package com.mvb.client;

import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.android.gms.maps.model.LatLng;
import com.mvb.client.NoteFragment.OnListFragmentInteractionListener;
import com.mvb.client.dummy.DummyContent.DummyItem;
import com.mvb.client.models.Note;

import java.util.List;

/**
 * {@link RecyclerView.Adapter} that can display a {@link DummyItem} and makes a call to the
 * specified {@link OnListFragmentInteractionListener}.
 * TODO: Replace the implementation with code for your data type.
 */
public class MyNoteRecyclerViewAdapter extends RecyclerView.Adapter<MyNoteRecyclerViewAdapter.ViewHolder> {

    private final List<Note> mValues;
    private final OnListFragmentInteractionListener mListener;

    public MyNoteRecyclerViewAdapter(List<Note> items, OnListFragmentInteractionListener listener) {
        mValues = items;
        mListener = listener;
        //Log.e("inside constructor","NotesAdapter");
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.mynote, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        holder.mItem = mValues.get(position);
        //Log.e("inside onBindViewHolder","onBindViewHolder");
        //Log.e("inside onBindViewHolder",mValues.get(position).getId()+"");
        holder.mIdView.setText(mValues.get(position).getId()+"");
        holder.mContentView.setText(mValues.get(position).getContent());
        holder.mTitleView.setText(mValues.get(position).getTitle());
        holder.mCreatedView.setText(mValues.get(position).getCreated());
        holder.mShopNameView.setText(mValues.get(position).getShopname());

        holder.mView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (null != mListener) {
                    // Notify the active callbacks interface (the activity, if the
                    // fragment is attached to one) that an item has been selected.
                    mListener.onListFragmentInteraction(holder.mItem);
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return mValues.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public final View mView;
        public final TextView mIdView;
        public final TextView mContentView;
        public final TextView mTitleView;
        public final TextView mCreatedView;
        public final TextView mShopNameView;
        public final RelativeLayout mNotesListView;
       // public final TextView mContevntView;
        public Note mItem;

        public ViewHolder(View view) {
            super(view);
            mView = view;
            mIdView = (TextView) view.findViewById(R.id.noteshopid);
            mContentView = (TextView) view.findViewById(R.id.mynotecontent);
            mTitleView = (TextView) view.findViewById(R.id.mynotetitle);
            mCreatedView = (TextView) view.findViewById(R.id.mynotetime);
            mShopNameView = (TextView) view.findViewById(R.id.noteshopname);
            mNotesListView=(RelativeLayout) view.findViewById(R.id.mynoteslist);
            mNotesListView.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View v) {
                    Log.e("ItemClikc", "");
                   // ((MainActivity) parent.getContext()).setMapToshop(new LatLng(shop.getLat(), shop.getLon()),position);
                }


            });

        }



        @Override
        public String toString() {
            return super.toString() + " '" + mContentView.getText() + "'";
        }
    }
}
