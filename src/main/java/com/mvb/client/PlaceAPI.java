package com.mvb.client;

import android.location.Location;
import android.util.Log;

import com.google.android.gms.maps.model.LatLng;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;
import java.util.ArrayList;

public class PlaceAPI {
	 
    private static final String TAG = PlaceAPI.class.getSimpleName();
 
    private static final String PLACES_API_BASE = "https://maps.googleapis.com/maps/api/place";
    private static final String GEOCODE_API_BASE = "https://maps.googleapis.com/maps/api/geocode";
    private static final String TYPE_AUTOCOMPLETE = "/autocomplete";
    private static final String OUT_JSON = "/json";
 
    private static final String API_KEY = "AIzaSyBCAT4wE8llJWlk_nrnfj28uRQvLr3vxPA";
 
    public ArrayList<String> autocomplete (String input) {
        ArrayList<String> resultList = null;
 
        HttpURLConnection conn = null;
        StringBuilder jsonResults = new StringBuilder();
 
        try {
            StringBuilder sb = new StringBuilder(PLACES_API_BASE + TYPE_AUTOCOMPLETE + OUT_JSON);
            sb.append("?key=" + API_KEY);
            sb.append("&types=(regions)");
            sb.append("&input=" + URLEncoder.encode(input, "utf8"));

            URL url = new URL(sb.toString());
            conn = (HttpURLConnection) url.openConnection();
            InputStreamReader in = new InputStreamReader(conn.getInputStream());
            Log.e(TAG, sb.toString());
            // Load the results into a StringBuilder
            int read;
            char[] buff = new char[1024];
            while ((read = in.read(buff)) != -1) {
                jsonResults.append(buff, 0, read);
            }

            Log.e(TAG, jsonResults.toString());
        } catch (MalformedURLException e) {
            Log.e(TAG, "Error processing Places API URL", e);
            return resultList;
        } catch (IOException e) {
            Log.e(TAG, "Error connecting to Places API", e);
            return resultList;
        } finally {
            if (conn != null) {
                conn.disconnect();
            }
        }
 
        try {
            // Log.d(TAG, jsonResults.toString());

            // Create a JSON object hierarchy from the results
            JSONObject jsonObj = new JSONObject(jsonResults.toString());
            JSONArray predsJsonArray = jsonObj.getJSONArray("predictions");

            // Extract the Place descriptions from the results
            resultList = new ArrayList<String>(predsJsonArray.length());
            for (int i = 0; i < predsJsonArray.length(); i++) {
                resultList.add(predsJsonArray.getJSONObject(i).getString("description"));
            }

        } catch (JSONException e) {
            Log.e(TAG, "Cannot process JSON results", e);
        }
 
        return resultList;
    }
    
    public LatLng getLatLon (String input) {
        ArrayList<String> resultList = null;
        LatLng my_latlong=null;
        HttpURLConnection conn = null;
        StringBuilder jsonResults = new StringBuilder();
 
        try {
            StringBuilder sb = new StringBuilder(GEOCODE_API_BASE + OUT_JSON);
            sb.append("?key=" + API_KEY);
            sb.append("&address=" + URLEncoder.encode(input, "utf8"));
            Log.e(TAG, sb.toString());
            URL url = new URL(sb.toString());
            conn = (HttpURLConnection) url.openConnection();
            InputStreamReader in = new InputStreamReader(conn.getInputStream());
           
            // Load the results into a StringBuilder
            int read;
            char[] buff = new char[1024];
            while ((read = in.read(buff)) != -1) {
                jsonResults.append(buff, 0, read);
            }
        } catch (MalformedURLException e) {
            Log.e(TAG, "Error processing Places API URL", e);
            return my_latlong;
        } catch (IOException e) {
            Log.e(TAG, "Error connecting to Places API", e);
            return my_latlong;
        } finally {
            if (conn != null) {
                conn.disconnect();
            }
        }
 
        try {
            // Log.d(TAG, jsonResults.toString());
 
            // Create a JSON object hierarchy from the results
            JSONObject jsonObj = new JSONObject(jsonResults.toString());
            JSONArray predsJsonArray = jsonObj.getJSONArray("results");
            JSONObject jsonGeometryObj =(JSONObject) predsJsonArray.getJSONObject(0).get("geometry");
            JSONObject jsonLocationObj =(JSONObject)jsonGeometryObj.get("location");
            Log.e("jsonLocationObj",jsonLocationObj.getDouble("lat")+" "+jsonLocationObj.getDouble("lng"));
            my_latlong = new LatLng(jsonLocationObj.getDouble("lat"),jsonLocationObj.getDouble("lng"));
            
        } catch (JSONException e) {
            Log.e(TAG, "Cannot process JSON results", e);
        }
         return my_latlong;
    }

    public ArrayList<String> shopsearch (String input,Location loc) {
        ArrayList<String> resultList = null;

        HttpURLConnection conn = null;

        StringBuilder jsonResultsb = new StringBuilder();

        try {
            //shops searching
            StringBuilder sbb = new StringBuilder("http://gpl4you.com/mvb/index.php/shops/getSearchShopsJson");
            sbb.append("?input=" + URLEncoder.encode(input, "utf8"));
            sbb.append("&lat=" + URLEncoder.encode(String.valueOf(loc.getLatitude()), "utf8"));
            sbb.append("&lon=" + URLEncoder.encode(String.valueOf(loc.getLongitude()), "utf8"));

            URL urlb = new URL(sbb.toString());
            conn = (HttpURLConnection) urlb.openConnection();
            InputStreamReader inb = new InputStreamReader(conn.getInputStream());
            Log.e(TAG, sbb.toString());
            // Load the results into a StringBuilder
            int readb;
            char[] buffb = new char[1024];
            while ((readb = inb.read(buffb)) != -1) {
                jsonResultsb.append(buffb, 0, readb);
            }


            Log.e(TAG, jsonResultsb.toString());
        } catch (MalformedURLException e) {
            Log.e(TAG, "Error processing Places API URL", e);
            return resultList;
        } catch (IOException e) {
            Log.e(TAG, "Error connecting to Places API", e);
            return resultList;
        } finally {
            if (conn != null) {
                conn.disconnect();
            }
        }

        try {
            // Log.d(TAG, jsonResults.toString());

            JSONObject jsonObjb = new JSONObject(jsonResultsb.toString());
            JSONArray predsJsonArrayb = jsonObjb.getJSONArray("shops");
            // Extract the Place descriptions from the results
            resultList = new ArrayList<String>(predsJsonArrayb.length());

            for (int i = 0; i < predsJsonArrayb.length(); i++) {
                //Shop shop= new Shop();
                //shop.setId(predsJsonArray.getJSONObject(i).getInt("id"));
                resultList.add(predsJsonArrayb.getJSONObject(i).getString("name"));
            }

        } catch (JSONException e) {
            Log.e(TAG, "Cannot process JSON results", e);
        }

        return resultList;
    }


    public ArrayList<Shop> getShop (String input,String lastSearch,Location loc) {

        HttpURLConnection conn = null;
        StringBuilder jsonResults = new StringBuilder();
        ArrayList<Shop> resultList = new ArrayList<Shop>();
        try {
            StringBuilder sb = new StringBuilder("http://gpl4you.com/mvb/index.php/shops/getShopData");
            sb.append("?input=" + URLEncoder.encode(input, "utf8"));
            sb.append("&last=" + URLEncoder.encode(lastSearch, "utf8"));
            sb.append("&lat=" + URLEncoder.encode(String.valueOf(loc.getLatitude()), "utf8"));
            sb.append("&lon=" + URLEncoder.encode(String.valueOf(loc.getLongitude()), "utf8"));
            Log.e(TAG, sb.toString());
            URL url = new URL(sb.toString());
            conn = (HttpURLConnection) url.openConnection();
            InputStreamReader in = new InputStreamReader(conn.getInputStream());

            // Load the results into a StringBuilder
            int read;
            char[] buff = new char[1024];
            while ((read = in.read(buff)) != -1) {
                jsonResults.append(buff, 0, read);
            }
        } catch (MalformedURLException e) {
            Log.e(TAG, "Error processing Places API URL", e);
            return resultList;
        } catch (IOException e) {
            Log.e(TAG, "Error connecting to Places API", e);
            return resultList;
        } finally {
            if (conn != null) {
                conn.disconnect();
            }
        }

        try {
            JSONObject jsonObj = new JSONObject(jsonResults.toString());
            JSONArray predsJsonArray = jsonObj.getJSONArray("shops");
            for (int i = 0; i < predsJsonArray.length(); i++) {
                Shop shop= new Shop();
                shop.setId(predsJsonArray.getJSONObject(i).getInt("id"));
                shop.setName(predsJsonArray.getJSONObject(i).getString("name"));
                shop.setAddress(predsJsonArray.getJSONObject(i).getString("address"));
                shop.setLat(predsJsonArray.getJSONObject(i).getDouble("latitude"));
                shop.setLon(predsJsonArray.getJSONObject(i).getDouble("longitude"));
                shop.setMobile(predsJsonArray.getJSONObject(i).getString("mobile"));
                shop.setMaxTime(predsJsonArray.getJSONObject(i).getString("max_time")+" mins");
                shop.setStartTime(predsJsonArray.getJSONObject(i).getString("starttime"));
                shop.setEndTime(predsJsonArray.getJSONObject(i).getString("endtime"));
                shop.setCharge(predsJsonArray.getJSONObject(i).getString("charge"));
                shop.setMinOrder(predsJsonArray.getJSONObject(i).getString("minimum_order"));
                shop.setMaxDistance(predsJsonArray.getJSONObject(i).getString("max_distance"));
                shop.setMainCategory(predsJsonArray.getJSONObject(i).getString("catg"));
                shop.setDistance(MyDatabase.round(predsJsonArray.getJSONObject(i).getDouble("distance"),2));
                //shop.setDistance(MyDatabase.round(2.0,2));
                Log.e(TAG, shop.getName());
                resultList.add(shop);
            }
        } catch (JSONException e) {
            Log.e(TAG, "Cannot process JSON results", e);
        }
        return resultList;
    }
}
