package com.mvb.client;

import android.app.Dialog;
import android.app.DialogFragment;
import android.app.TimePickerDialog;
import android.os.Bundle;
import android.util.Log;
import android.widget.TextView;
import android.widget.TimePicker;

import java.text.DateFormat;
import java.util.Calendar;

public  class TimePickerFragment extends DialogFragment
                            implements TimePickerDialog.OnTimeSetListener {

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        // Use the current time as the default values for the picker
        final Calendar c = Calendar.getInstance();
        int hour = c.get(Calendar.HOUR_OF_DAY);
        int minute = c.get(Calendar.MINUTE);

        // Create a new instance of TimePickerDialog and return it
        return new TimePickerDialog(getActivity(), this, hour, minute,
                android.text.format.DateFormat.is24HourFormat(getActivity()));
    }

    public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
        // Do something with the time chosen by the user
        //((ShopAdd)getActivity()).shopStartDateSet(hourOfDay, minute);
        Log.e("Time", hourOfDay + ":" + minute);
    //    TextView txtStartDate = (TextView)getActivity().findViewById(R.id.addShopStartTimeLabel);
        String min="";
        min=""+minute;
        if(minute<10){
            min="0"+min;
        }
        ((MainActivity)getActivity()).setstarttime(hourOfDay + ":" + min);
     //   txtStartDate.setText(hourOfDay + ":" + min);
    }
}