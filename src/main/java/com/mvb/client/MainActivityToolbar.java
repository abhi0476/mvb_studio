package com.mvb.client;


import android.content.Context;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.mvb.client.models.Category;

import java.util.ArrayList;
/**
 * The Class MainActivity.
 */
public class MainActivityToolbar  extends AppCompatActivity {

	 // Title navigation Spinner data
    private ArrayList<SpinnerNavItem> navSpinner;
    Spinner spinner_nav;
    private MenuItem mSearchAction;
    private boolean isSearchOpened = false;
    private EditText edtSeach;
    // Navigation adapter
   
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_mainn);
		Toolbar mToolbar = (Toolbar) findViewById(R.id.toolbar);
		spinner_nav = (Spinner) findViewById(R.id.spinner_nav);
		if(mToolbar != null) {
			Log.i("not null","not null");
            setSupportActionBar(mToolbar);
            getSupportActionBar().setTitle("MVB");
            getSupportActionBar().setHomeButtonEnabled(true);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            
        }
		addItemsToSpinner();
	}
	// add items into spinner dynamically
	 public void addItemsToSpinner() {

	  ArrayList<Category> list = new ArrayList<Category>();
//	  list.add("Top News");
//	  list.add("Politics");
//	  list.add("Business");
//	  list.add("Sports");
//	  list.add("Movies");

	  // Custom ArrayAdapter with spinner item layout to set popup background

	  CustomSpinnerAdapter spinAdapter = new CustomSpinnerAdapter(
	    getApplicationContext(), list);

	  
	  
	  // Default ArrayAdapter with default spinner item layout, getting some
	  // view rendering problem in lollypop device, need to test in other
	  // devices

	  /*
	   * ArrayAdapter<String> spinAdapter = new ArrayAdapter<String>(this,
	   * android.R.layout.simple_spinner_item, list);
	   * spinAdapter.setDropDownViewResource
	   * (android.R.layout.simple_spinner_dropdown_item);
	   */

	  spinner_nav.setAdapter(spinAdapter);

	  spinner_nav.setOnItemSelectedListener(new OnItemSelectedListener() {

	   @Override
	   public void onItemSelected(AdapterView<?> adapter, View v,
	     int position, long id) {
	    // On selecting a spinner item
	    String item = adapter.getItemAtPosition(position).toString();

	    // Showing selected spinner item
	    Toast.makeText(getApplicationContext(), "Selected  : " + item,
	      Toast.LENGTH_LONG).show();
	   }

	   @Override
	   public void onNothingSelected(AdapterView<?> arg0) {
	    // TODO Auto-generated method stub

	   }
	  });

	 }
	 @Override
	  public boolean onPrepareOptionsMenu(Menu menu) {
	    //mSearchAction = menu.findItem(R.id.action_search);
	    return super.onPrepareOptionsMenu(menu);
	  }
	@Override
	  public boolean onCreateOptionsMenu(Menu menu) {
	    MenuInflater inflater = getMenuInflater();
	    inflater.inflate(R.menu.main, menu);
//	    // Associate searchable configuration with the SearchView
//        SearchManager searchManager = (SearchManager) getSystemService(Context.SEARCH_SERVICE);
//        SearchView searchView = (SearchView) menu.findItem(R.id.action_search)
//                .getActionView();
//        searchView.setSearchableInfo(searchManager
//                .getSearchableInfo(getComponentName()));
 
//        return super.onCreateOptionsMenu(menu);
	    return true;
	  } 
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
	    switch (item.getItemId()) {
//	        case R.id.action_settings:
//	            // User chose the "Settings" item, show the app settings UI...
//	            return true;
//
//	        case R.id.action_favorite:
//	            // User chose the "Favorite" action, mark the current item
//	            // as a favorite...
//	            return true;
//	        case R.id.action_search:
//	            handleMenuSearch();
//	            return true;

	        default:
	            // If we got here, the user's action was not recognized.
	            // Invoke the superclass to handle it.
	            return super.onOptionsItemSelected(item);

	    }
	}
	protected void handleMenuSearch(){
	    ActionBar action = getSupportActionBar(); //get the actionbar

	    if(isSearchOpened){ //test if the search is open

	        action.setDisplayShowCustomEnabled(false); //disable a custom view inside the actionbar
	        action.setDisplayShowTitleEnabled(true); //show the title in the action bar

	        //hides the keyboard
	        InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
	        imm.hideSoftInputFromWindow(edtSeach.getWindowToken(), 0);

	        //add the search icon in the action bar
	        mSearchAction.setIcon(getResources().getDrawable(R.drawable.ic_action_search));

	        isSearchOpened = false;
	    } else { //open the search entry

	        action.setDisplayShowCustomEnabled(true); //enable it to display a
	        // custom view in the action bar.
	        action.setCustomView(R.layout.search_bar);//add the custom view
	        action.setDisplayShowTitleEnabled(false); //hide the title

	        edtSeach = (EditText)action.getCustomView().findViewById(R.id.edtSearch); //the text editor

	        //this is a listener to do a search when the user clicks on search button
	        edtSeach.setOnEditorActionListener(new TextView.OnEditorActionListener() {
	            @Override
	            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
	                if (actionId == EditorInfo.IME_ACTION_SEARCH) {
	                    doSearch();
	                    return true;
	                }
	                return false;
	            }

	        });


	        edtSeach.requestFocus();

	        //open the keyboard focused in the edtSearch
	        InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
	        imm.showSoftInput(edtSeach, InputMethodManager.SHOW_IMPLICIT);


	        //add the close icon
	        mSearchAction.setIcon(getResources().getDrawable(R.drawable.ic_checkout_input_ok));

	        isSearchOpened = true;
	    }
	}
	@Override
	public void onBackPressed() {
	  if(isSearchOpened) {
	    handleMenuSearch();
	    return;
	  }
	  super.onBackPressed();
	}
	private void doSearch() {
		//
		}
}
