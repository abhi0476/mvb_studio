package com.mvb.client;

import android.content.Context;
import android.location.Location;
import android.widget.ArrayAdapter;
import android.widget.Filter;
import android.widget.Filterable;

import java.util.ArrayList;
class PlacesAutoCompleteAdapter extends ArrayAdapter<String> implements Filterable {
	 
    ArrayList<String> resultList;
 
    Context mContext;
    int mResource;
    Location loc;
    PlaceAPI mPlaceAPI = new PlaceAPI();
 
    public PlacesAutoCompleteAdapter(Context context, int resource) {
        super(context, resource);
        mContext = context;
        mResource = resource;
    }
 
    @Override
    public int getCount() {
        // Last item will be the footer
        return resultList.size();
    }
 
    @Override
    public String getItem(int position) {
        return resultList.get(position);
    }
 
    @Override
    public Filter getFilter() {
        Filter filter = new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence constraint) {
                FilterResults filterResults = new FilterResults();
                if (constraint != null) {
                    resultList = mPlaceAPI.shopsearch(constraint.toString(),MainActivity.lastLocation);
                    resultList.addAll(mPlaceAPI.autocomplete(constraint.toString()));
                    MainActivity.lastSearchedString=constraint.toString();
                    filterResults.values = resultList;
                    filterResults.count = resultList.size();
                }
 
                return filterResults;
            }
 
            @Override
            protected void publishResults(CharSequence constraint, FilterResults results) {
                if (results != null && results.count > 0) {
                    notifyDataSetChanged();
                }
                else {
                    notifyDataSetInvalidated();
                }
            }
        };
 
        return filter;
    }
}
