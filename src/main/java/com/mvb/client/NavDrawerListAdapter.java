package com.mvb.client;

import android.app.Activity;
import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.mvb.client.models.NavDrawerItem;

import java.util.ArrayList;

public class NavDrawerListAdapter extends BaseAdapter {

    private Context context;
    private ArrayList<NavDrawerItem> navDrawerItems;

    public NavDrawerListAdapter(Context context, ArrayList<NavDrawerItem> navDrawerItems){
        this.context = context;
        this.navDrawerItems = navDrawerItems;
    }

    @Override
    public int getCount() {
        return navDrawerItems.size();
    }

    @Override
    public Object getItem(int position) {
        return navDrawerItems.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (convertView == null) {
            LayoutInflater mInflater = (LayoutInflater)
                    context.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
            convertView = mInflater.inflate(R.layout.drawer_list_item, null);
        }

        ImageView imgIcon = (ImageView) convertView.findViewById(R.id.icon);
        TextView txtTitle = (TextView) convertView.findViewById(R.id.title);
        TextView txtCount = (TextView) convertView.findViewById(R.id.counter);
        RelativeLayout navlist = (RelativeLayout) convertView.findViewById(R.id.navdrawerlist);

        imgIcon.setImageResource(navDrawerItems.get(position).getIcon());
        txtTitle.setText(navDrawerItems.get(position).getTitle());

        // displaying count
        // check whether it set visible or not
        if(navDrawerItems.get(position).getCounterVisibility()){
            txtCount.setText(navDrawerItems.get(position).getCount());
        }else{
            // hide the counter view
            txtCount.setVisibility(View.GONE);
        }
   /*     navlist.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                Log.e("ItemClikc", "");
                // ((MainActivity) parent.getContext()).setMapToshop(new LatLng(shop.getLat(), shop.getLon()),position);
            }


        });

        txtTitle.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                Log.e("ccItemClikc", "");
                // ((MainActivity) parent.getContext()).setMapToshop(new LatLng(shop.getLat(), shop.getLon()),position);
            }


        });
        imgIcon.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                Log.e("ccItemClikc", "");
                // ((MainActivity) parent.getContext()).setMapToshop(new LatLng(shop.getLat(), shop.getLon()),position);
            }


        }); */

        return convertView;
    }

}