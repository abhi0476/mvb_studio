package com.mvb.client.dao;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.os.StrictMode;
import android.provider.Settings;
import android.util.Log;

import com.google.android.gms.maps.model.LatLng;
import com.mvb.client.MyDatabase;
import com.mvb.client.Shop;
import com.mvb.client.models.Category;
import com.mvb.client.models.Note;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.List;

public class GetDataFromServer {
	 private static final String TAG = GetDataFromServer.class.getSimpleName();
	 
	    private static final String SERVER_ROOT ="http://gpl4you.com/mvb/index.php";// "http://mvb.gpl4you.com/index.php";
	    private static final String GET_SHOP = "/shops/getShopsJson";
	private static final String GET_CATG = "/shops/getCategory";
	private static final String GET_MY_NOTES = "/shops/getMyNotes";
	private static final String GET_SHOP_NOTES = "/shops/getShopNotes";
	private static final String GET_FAVSHOPS = "/shops/getFavShops";
	    private static final String TYPE_AUTOCOMPLETE = "/autocomplete";
	    private static final String OUT_JSON = "/json";
	 
	    private static final String API_KEY = "AIzaSyDvS1lrSwyXJjORO9Lsrg5euLfSKcB4AvQ";
	ProgressDialog pd ;
	    public ArrayList<Shop> getShopList (LatLng location) {
	        ArrayList<Shop> resultList = null;
	 
	        HttpURLConnection conn = null;
	        StringBuilder jsonResults = new StringBuilder();
	 
	        try {
	            StringBuilder sb = new StringBuilder(SERVER_ROOT + GET_SHOP );
	            sb.append("?lat=" + URLEncoder.encode(String.valueOf(location.latitude), "utf8"));
	            sb.append("&lon=" + URLEncoder.encode(String.valueOf(location.longitude), "utf8"));
	 
	            URL url = new URL(sb.toString());
	            conn = (HttpURLConnection) url.openConnection();
	            InputStreamReader in = new InputStreamReader(conn.getInputStream());
	            Log.e(TAG, sb.toString());
	            // Load the results into a StringBuilder
	            int read;
	            char[] buff = new char[1024];
	            while ((read = in.read(buff)) != -1) {
	                jsonResults.append(buff, 0, read);
	            }
	        } catch (MalformedURLException e) {
	            Log.e(TAG, "Error processing Places API URL", e);
	            return resultList;
	        } catch (IOException e) {
	            Log.e(TAG, "Error connecting to Places API", e);
	            return resultList;
	        } finally {
	            if (conn != null) {
	                conn.disconnect();
	            }
	        }
	 
	        try {
	             //Log.e(TAG, jsonResults.toString());
	            // System.out.println(jsonResults.toString());
	            // Create a JSON object hierarchy from the results
	            JSONObject jsonObj = new JSONObject(jsonResults.toString());
	            JSONArray predsJsonArray = jsonObj.getJSONArray("shops");
	 
	            // Extract the Place descriptions from the results
	            resultList = new ArrayList<Shop>();//predsJsonArray.length()
	            for (int i = 0; i < predsJsonArray.length(); i++) {
	            	Shop shop= new Shop();
	            	shop.setId(predsJsonArray.getJSONObject(i).getInt("id"));
	            	shop.setName(predsJsonArray.getJSONObject(i).getString("name"));
	            	shop.setAddress(predsJsonArray.getJSONObject(i).getString("address"));
	            	shop.setLat(predsJsonArray.getJSONObject(i).getDouble("latitude"));
	            	shop.setLon(predsJsonArray.getJSONObject(i).getDouble("longitude"));
	            	shop.setMobile(predsJsonArray.getJSONObject(i).getString("mobile"));
	            	shop.setMaxTime(predsJsonArray.getJSONObject(i).getString("max_time")+" mins");
	            	shop.setStartTime(predsJsonArray.getJSONObject(i).getString("starttime"));
	            	shop.setEndTime(predsJsonArray.getJSONObject(i).getString("endtime"));
	            	shop.setCharge(predsJsonArray.getJSONObject(i).getString("charge"));
	            	shop.setMinOrder(predsJsonArray.getJSONObject(i).getString("minimum_order"));
	            	shop.setMaxDistance(predsJsonArray.getJSONObject(i).getString("max_distance"));
	            	shop.setMainCategory(predsJsonArray.getJSONObject(i).getString("catg"));
	            	shop.setDistance(MyDatabase.round(predsJsonArray.getJSONObject(i).getDouble("distance"),2));
	            	
	            	Log.e(TAG, shop.getName());
	                resultList.add(shop);
	            }
	        } catch (JSONException e) {
	            Log.e(TAG, "Cannot process JSON results", e);
	        }
	 
	        return resultList;
	    }
	    
	    public ArrayList<Shop> getShopListCategory (LatLng location,String category) {
	        ArrayList<Shop> resultList = null;
	 
	        HttpURLConnection conn = null;
	        StringBuilder jsonResults = new StringBuilder();
	 
	        try {
	            StringBuilder sb = new StringBuilder(SERVER_ROOT + GET_SHOP );
	            sb.append("?lat=" + URLEncoder.encode(String.valueOf(location.latitude), "utf8"));
	            sb.append("&lon=" + URLEncoder.encode(String.valueOf(location.longitude), "utf8"));
	            sb.append("&catg=" + URLEncoder.encode(category, "utf8"));
	 
	            URL url = new URL(sb.toString());
	            conn = (HttpURLConnection) url.openConnection();
	            InputStreamReader in = new InputStreamReader(conn.getInputStream());
	            Log.e(TAG, sb.toString());
	            // Load the results into a StringBuilder
	            int read;
	            char[] buff = new char[1024];
	            while ((read = in.read(buff)) != -1) {
	                jsonResults.append(buff, 0, read);
	            }
	        } catch (MalformedURLException e) {
	            Log.e(TAG, "Error processing Places API URL", e);
	            return resultList;
	        } catch (IOException e) {
	            Log.e(TAG, "Error connecting to Places API", e);
	            return resultList;
	        } finally {
	            if (conn != null) {
	                conn.disconnect();
	            }
	        }
	 
	        try {
	             //Log.e(TAG, jsonResults.toString());
	            // System.out.println(jsonResults.toString());
	            // Create a JSON object hierarchy from the results
	            JSONObject jsonObj = new JSONObject(jsonResults.toString());
	            JSONArray predsJsonArray = jsonObj.getJSONArray("shops");
	 
	            // Extract the Place descriptions from the results
	            resultList = new ArrayList<Shop>();//(predsJsonArray.length())
	            for (int i = 0; i < predsJsonArray.length()-1; i++) {
	            	Shop shop= new Shop();
	            	shop.setId(predsJsonArray.getJSONObject(i).getInt("id"));
	            	shop.setName(predsJsonArray.getJSONObject(i).getString("name"));
	            	shop.setAddress(predsJsonArray.getJSONObject(i).getString("address"));
	            	shop.setLat(predsJsonArray.getJSONObject(i).getDouble("latitude"));
	            	shop.setLon(predsJsonArray.getJSONObject(i).getDouble("longitude"));
	            	shop.setMobile(predsJsonArray.getJSONObject(i).getString("mobile"));
	            	shop.setMaxTime(predsJsonArray.getJSONObject(i).getString("max_time")+" mins");
	            	shop.setStartTime(predsJsonArray.getJSONObject(i).getString("starttime"));
	            	shop.setEndTime(predsJsonArray.getJSONObject(i).getString("endtime"));
	            	shop.setCharge(predsJsonArray.getJSONObject(i).getString("charge"));
	            	shop.setMinOrder(predsJsonArray.getJSONObject(i).getString("minimum_order"));
	            	shop.setMaxDistance(predsJsonArray.getJSONObject(i).getString("max_distance"));
	            	shop.setMainCategory(predsJsonArray.getJSONObject(i).getString("catg"));
	            	shop.setDistance(MyDatabase.round(predsJsonArray.getJSONObject(i).getDouble("distance"),2));
	            	
	            	Log.e(TAG, shop.getName());
	                resultList.add(shop);
	            }
	        } catch (JSONException e) {
	            Log.e(TAG, "Cannot process JSON results", e);
	        }
	 
	        return resultList;
	    }

	public ArrayList<Category> getCatgList () {
		ArrayList<Category> resultList = null;

		HttpURLConnection conn = null;
		StringBuilder jsonResults = new StringBuilder();

		try {
			StringBuilder sb = new StringBuilder(SERVER_ROOT + GET_CATG );

			URL url = new URL(sb.toString());
			conn = (HttpURLConnection) url.openConnection();
			InputStreamReader in = new InputStreamReader(conn.getInputStream());
			Log.e(TAG, sb.toString());
			// Load the results into a StringBuilder
			int read;
			char[] buff = new char[1024];
			while ((read = in.read(buff)) != -1) {
				jsonResults.append(buff, 0, read);
			}
		} catch (MalformedURLException e) {
			Log.e(TAG, "Error processing Places API URL", e);
			return resultList;
		} catch (IOException e) {
			Log.e(TAG, "Error connecting to Places API", e);
			return resultList;
		} finally {
			if (conn != null) {
				conn.disconnect();
			}
		}

		try {
			//Log.e(TAG, jsonResults.toString());
			// System.out.println(jsonResults.toString());
			// Create a JSON object hierarchy from the results
			JSONObject jsonObj = new JSONObject(jsonResults.toString());
			JSONArray predsJsonArray = jsonObj.getJSONArray("category");

			// Extract the Place descriptions from the results
			resultList = new ArrayList<Category>(predsJsonArray.length()+1);
			Category catg=new Category();
			catg.id=0;
			catg.name="All";
			resultList.add(catg);
			for (int i = 0; i < predsJsonArray.length(); i++) {
				Category catgg=new Category();

				catgg.setId(predsJsonArray.getJSONObject(i).getInt("id"));
				catgg.setName(predsJsonArray.getJSONObject(i).getString("name"));
				Log.e(TAG, catgg.getName());
				resultList.add(catgg);
			}
		} catch (JSONException e) {
			Log.e(TAG, "Cannot process JSON results", e);
		}

		return resultList;
	}


	public ArrayList<String> getFavShopList (int userId,Context appContext) {
		ArrayList<String> resultList = null;

		StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder()
				.permitAll().build();
		StrictMode.setThreadPolicy(policy);
		HttpURLConnection conn = null;
		StringBuilder jsonResults = new StringBuilder();

		try {
			StringBuilder sb = new StringBuilder(SERVER_ROOT + GET_FAVSHOPS );
			String android_id = Settings.Secure.getString(appContext.getContentResolver(),
					Settings.Secure.ANDROID_ID);
			sb.append("?device_id=" + URLEncoder.encode(String.valueOf(android_id), "utf8"));
			if(userId!=0){
				sb.append("&user_id=" + URLEncoder.encode(String.valueOf(userId), "utf8"));
			}


			URL url = new URL(sb.toString());
			conn = (HttpURLConnection) url.openConnection();
			InputStreamReader in = new InputStreamReader(conn.getInputStream());
			Log.e(TAG, sb.toString());
			// Load the results into a StringBuilder
			int read;
			char[] buff = new char[1024];
			while ((read = in.read(buff)) != -1) {
				jsonResults.append(buff, 0, read);
			}
		} catch (MalformedURLException e) {
			Log.e(TAG, "Error processing Places API URL", e);
			return resultList;
		} catch (IOException e) {
			Log.e(TAG, "Error connecting to Places API", e);
			return resultList;
		} finally {
			if (conn != null) {
				conn.disconnect();
			}
		}

		try {
			//Log.e(TAG, jsonResults.toString());
			// System.out.println(jsonResults.toString());
			// Create a JSON object hierarchy from the results
			JSONObject jsonObj = new JSONObject(jsonResults.toString());
			JSONArray predsJsonArray = jsonObj.getJSONArray("fav_shops");

			// Extract the Place descriptions from the results
			resultList = new ArrayList<String>(predsJsonArray.length()+1);

			for (int i = 0; i < predsJsonArray.length(); i++) {
				Log.e(TAG, predsJsonArray.getJSONObject(i).getString("shop_id"));
				resultList.add(predsJsonArray.getJSONObject(i).getString("shop_id"));
			}
		} catch (JSONException e) {
			Log.e(TAG, "Cannot process JSON results", e);
		}

		return resultList;
	}

	public ArrayList<Note> getMyNotesList (int shopId,int userId,Context appContext) {
		ArrayList<Note> resultList = null;
		//pd =new ProgressDialog(appContext) ;
		//pd.setMessage("Loading..");
		//pd.show();
		StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder()
				.permitAll().build();
		StrictMode.setThreadPolicy(policy);
		HttpURLConnection conn = null;
		StringBuilder jsonResults = new StringBuilder();

		try {
			StringBuilder sb = new StringBuilder(SERVER_ROOT + GET_SHOP_NOTES );
			String android_id = Settings.Secure.getString(appContext.getContentResolver(),
					Settings.Secure.ANDROID_ID);
			sb.append("?device_id=" + URLEncoder.encode(String.valueOf(android_id), "utf8"));
			sb.append("&shop_id=" + URLEncoder.encode(String.valueOf(shopId), "utf8"));
			if(userId!=0){
				sb.append("&user_id=" + URLEncoder.encode(String.valueOf(userId), "utf8"));
			}
			URL url = new URL(sb.toString());
			conn = (HttpURLConnection) url.openConnection();
			InputStreamReader in = new InputStreamReader(conn.getInputStream());
			Log.e(TAG, sb.toString());
			// Load the results into a StringBuilder
			int read;
			char[] buff = new char[1024];
			while ((read = in.read(buff)) != -1) {
				jsonResults.append(buff, 0, read);
			}
		} catch (MalformedURLException e) {
			Log.e(TAG, "Error processing Places API URL", e);
			return resultList;
		} catch (IOException e) {
			Log.e(TAG, "Error connecting to Places API", e);
			return resultList;
		} finally {
			if (conn != null) {
				conn.disconnect();
				//pd.dismiss();
			}
		}

		try {
			//Log.e(TAG, jsonResults.toString());
			// System.out.println(jsonResults.toString());
			// Create a JSON object hierarchy from the results
			JSONObject jsonObj = new JSONObject(jsonResults.toString());
			JSONArray predsJsonArray = jsonObj.getJSONArray("shop_notes");

			// Extract the Place descriptions from the results
			resultList = new ArrayList<Note>(predsJsonArray.length()+1);
			Note catg=new Note();

			for (int i = 0; i < predsJsonArray.length(); i++) {
				Note note=new Note();
				note.setId(predsJsonArray.getJSONObject(i).getInt("id"));
				note.setTitle(predsJsonArray.getJSONObject(i).getString("title"));
				note.setName(predsJsonArray.getJSONObject(i).getString("name"));
				note.setContent(predsJsonArray.getJSONObject(i).getString("content"));
				note.setShopId(shopId);
				note.setCreated(predsJsonArray.getJSONObject(i).getString("created"));
				note.setIs_public(predsJsonArray.getJSONObject(i).getInt("is_public"));
				note.setOrderby(predsJsonArray.getJSONObject(i).getInt("orderby"));
				Log.e(TAG, note.getName());
				resultList.add(note);
			}
		} catch (JSONException e) {
			Log.e(TAG, "Cannot process JSON results", e);
		}

		return resultList;
	}

	public ArrayList<Note> getMyNotes (int userId,Context appContext) {
		ArrayList<Note> resultList = null;
		//pd =new ProgressDialog(appContext) ;
		//pd.setMessage("Loading..");
		//pd.show();
		StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder()
				.permitAll().build();
		StrictMode.setThreadPolicy(policy);
		HttpURLConnection conn = null;
		StringBuilder jsonResults = new StringBuilder();

		try {
			StringBuilder sb = new StringBuilder(SERVER_ROOT + GET_MY_NOTES );
			String android_id = Settings.Secure.getString(appContext.getContentResolver(),
					Settings.Secure.ANDROID_ID);
			sb.append("?device_id=" + URLEncoder.encode(String.valueOf(android_id), "utf8"));
			if(userId!=0){
				sb.append("&user_id=" + URLEncoder.encode(String.valueOf(userId), "utf8"));
			}
			URL url = new URL(sb.toString());
			conn = (HttpURLConnection) url.openConnection();
			InputStreamReader in = new InputStreamReader(conn.getInputStream());
			Log.e(TAG, sb.toString());
			// Load the results into a StringBuilder
			int read;
			char[] buff = new char[1024];
			while ((read = in.read(buff)) != -1) {
				jsonResults.append(buff, 0, read);
			}
		} catch (MalformedURLException e) {
			Log.e(TAG, "Error processing Places API URL", e);
			return resultList;
		} catch (IOException e) {
			Log.e(TAG, "Error connecting to Places API", e);
			return resultList;
		} finally {
			if (conn != null) {
				conn.disconnect();
				//pd.dismiss();
			}
		}

		try {
			//Log.e(TAG, jsonResults.toString());
			// System.out.println(jsonResults.toString());
			// Create a JSON object hierarchy from the results
			JSONObject jsonObj = new JSONObject(jsonResults.toString());
			JSONArray predsJsonArray = jsonObj.getJSONArray("my_notes");

			// Extract the Place descriptions from the results
			resultList = new ArrayList<Note>(predsJsonArray.length()+1);
			Note catg=new Note();

			for (int i = 0; i < predsJsonArray.length(); i++) {
				Note note=new Note();
				note.setId(predsJsonArray.getJSONObject(i).getInt("id"));
				note.setTitle(predsJsonArray.getJSONObject(i).getString("title"));
				note.setName(predsJsonArray.getJSONObject(i).getString("name"));
				note.setContent(predsJsonArray.getJSONObject(i).getString("content"));
				note.setShopId(Integer.parseInt(predsJsonArray.getJSONObject(i).getString("shop_id")));
				note.setShopname(predsJsonArray.getJSONObject(i).getString("shopname"));
				note.setCreated(predsJsonArray.getJSONObject(i).getString("created"));
				note.setIs_public(predsJsonArray.getJSONObject(i).getInt("is_public"));
				note.setOrderby(predsJsonArray.getJSONObject(i).getInt("orderby"));
				Log.e(TAG, note.getName());
				resultList.add(note);
			}
		} catch (JSONException e) {
			Log.e(TAG, "Cannot process JSON results", e);
		}

		return resultList;
	}
	public void postshopdata(String sn,String sa,String sat,String set,String lat,String lon,String spc,String ssc,String sdc,String smt,String smo){
		class SendPostReqAsyncTask extends AsyncTask<String, Void, String> {

			@Override
			protected String doInBackground(String... params) {
				String shopName = params[0];
				String shopAddress = params[1];
				String shopStartTime = params[2];
				String shopEndTime = params[3];
				String lat = params[4];
				String lon = params[5];
				String primaryContact = params[6];
				String secondaryContact = params[7];
				String shopDelCharge = params[8];
				String shopMaxTime = params[9];
				String shopMinOrder = params[10];


				HttpClient httpClient = new DefaultHttpClient();
				String url="http://www.gpl4you.com/mvb/addshop.php?";
				url=url+"shopName="+shopName;
				url=url+"&shopAddress="+shopAddress;
				url=url+"&shopStartTime="+shopStartTime;
				url=url+"&shopEndTime="+shopEndTime;
				url=url+"&latitude="+shopName;
				url=url+"&longitude="+shopName;
				url=url+"&mobile="+primaryContact;
				if(!secondaryContact.equalsIgnoreCase("")){
					url=url+",="+secondaryContact;
				}
				url=url+"&shopDelCharge="+shopDelCharge;
				url=url+"&shopMaxTime="+shopMaxTime;
				url=url+"&shopMinOrder="+shopMinOrder;

				// In a POST request, we don't pass the values in the URL.
				//Therefore we use only the web page URL as the parameter of the HttpPost argument
				HttpPost httpPost = new HttpPost("http://www.gpl4you.com/mvb/addshop.php");

				// Because we are not passing values over the URL, we should have a mechanism to pass the values that can be
				//uniquely separate by the other end.
				//To achieve that we use BasicNameValuePair
				//Things we need to pass with the POST request
				BasicNameValuePair shopNamev = new BasicNameValuePair("shopName", shopName);
				BasicNameValuePair shopAddressv = new BasicNameValuePair("shopAddress", shopAddress);
				BasicNameValuePair shopStartTimev = new BasicNameValuePair("shopStartTime", shopStartTime);
				BasicNameValuePair shopEndTimev = new BasicNameValuePair("shopEndTime", shopEndTime);
				BasicNameValuePair shopPrimaryContactv = new BasicNameValuePair("shopPrimaryContact", primaryContact);
				BasicNameValuePair shopSecondaryContactv = new BasicNameValuePair("shopSecondaryContact", secondaryContact);
				BasicNameValuePair shopDelChargev = new BasicNameValuePair("shopDelCharge", shopDelCharge);
				BasicNameValuePair shopMaxTimev = new BasicNameValuePair("shopMaxTime", shopMaxTime);
				BasicNameValuePair shopMinOrderv = new BasicNameValuePair("shopMinOrder", shopMinOrder);
				BasicNameValuePair latitudev = new BasicNameValuePair("latitude", lat);
				BasicNameValuePair longitudev = new BasicNameValuePair("longitude", lon);

				// We add the content that we want to pass with the POST request to as name-value pairs
				//Now we put those sending details to an ArrayList with type safe of NameValuePair
				List<NameValuePair> nameValuePairList = new ArrayList<NameValuePair>();
				nameValuePairList.add(shopNamev);
				nameValuePairList.add(shopAddressv);
				nameValuePairList.add(shopStartTimev);
				nameValuePairList.add(shopEndTimev);
				nameValuePairList.add(shopPrimaryContactv);
				nameValuePairList.add(shopSecondaryContactv);

				nameValuePairList.add(shopDelChargev);
				nameValuePairList.add(shopMaxTimev);
				nameValuePairList.add(shopMinOrderv);
				nameValuePairList.add(latitudev);
				nameValuePairList.add(longitudev);

				try {
					// UrlEncodedFormEntity is an entity composed of a list of url-encoded pairs.
					//This is typically useful while sending an HTTP POST request.
					UrlEncodedFormEntity urlEncodedFormEntity = new UrlEncodedFormEntity(nameValuePairList);

					// setEntity() hands the entity (here it is urlEncodedFormEntity) to the request.
					httpPost.setEntity(urlEncodedFormEntity);

					try {
						// HttpResponse is an interface just like HttpPost.
						//Therefore we can't initialize them
						HttpResponse httpResponse = httpClient.execute(httpPost);

						// According to the JAVA API, InputStream constructor do nothing.
						//So we can't initialize InputStream although it is not an interface
						InputStream inputStream = httpResponse.getEntity().getContent();

						InputStreamReader inputStreamReader = new InputStreamReader(inputStream);

						BufferedReader bufferedReader = new BufferedReader(inputStreamReader);

						StringBuilder stringBuilder = new StringBuilder();

						String bufferedStrChunk = null;

						while((bufferedStrChunk = bufferedReader.readLine()) != null){
							stringBuilder.append(bufferedStrChunk);
						}
						Log.e("ppp",stringBuilder.toString());
						return stringBuilder.toString();

					} catch (ClientProtocolException cpe) {
						System.out.println("First Exception caz of HttpResponese :" + cpe);
						cpe.printStackTrace();
					} catch (IOException ioe) {
						System.out.println("Second Exception caz of HttpResponse :" + ioe);
						ioe.printStackTrace();
					}

				} catch (UnsupportedEncodingException uee) {
					System.out.println("An Exception given because of UrlEncodedFormEntity argument :" + uee);
					uee.printStackTrace();
				}

				return null;
			}
			@Override
			protected void onPostExecute(String result) {
				super.onPostExecute(result);
				Log.e("log",result);
				if(result==null){
					//  return;
				}
				if(result.equals("done")){
					//Toast.makeText(appContext, "HTTP POST is working...", Toast.LENGTH_LONG).show();
				}else{
					//Toast.makeText(appContext, "Invalid POST req...", Toast.LENGTH_LONG).show();
				}
			}


		}

			SendPostReqAsyncTask sendPostReqAsyncTask = new SendPostReqAsyncTask();
		sendPostReqAsyncTask.execute("ppppp","aaaaaa","09:09","12:08","23.98766","77.987654","9087654312","8907654321","8","9","8");

	}


}
