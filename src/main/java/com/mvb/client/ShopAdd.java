package com.mvb.client;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.DialogFragment;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.app.Fragment;
import android.os.Environment;
import android.os.Handler;
import android.os.Looper;
import android.os.StrictMode;
import android.provider.MediaStore;
import android.provider.Settings;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.mvb.client.library.MultiSelectionSpinner;


import com.android.volley.NetworkResponse;
import com.android.volley.Response;
import com.android.volley.VolleyError;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.URL;
import java.net.URLEncoder;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.List;
import java.util.Map;

import com.mvb.client.dao.GetDataFromServer;


public class ShopAdd extends AppCompatActivity implements MultiSelectionSpinner.OnMultipleItemsSelectedListener{
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;
    int REQUEST_CAMERA = 0, SELECT_FILE = 1;
    Button btnSelect;
    Button btnSubmit;
    ImageView ivImage;
    private int PICK_IMAGE_REQUEST = 1;

    private String UPLOAD_URL ="http://www.gpl4you.com/mvb/index.php/shops/shopCreate";

    private String KEY_IMAGE = "image";
    private String KEY_NAME = "name";



    @Override
    public void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.shop_add);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);

        String[] array = {"one", "two", "three", "four", "five", "six", "seven", "eight", "nine", "ten"};
        MultiSelectionSpinner multiSelectionSpinner = (MultiSelectionSpinner) findViewById(R.id.catgSpinner);
        multiSelectionSpinner.setItems(array);
        multiSelectionSpinner.setSelection(new int[]{2, 6});
        multiSelectionSpinner.setListener(this);

        btnSelect = (Button) findViewById(R.id.btnSelectPhoto);
        btnSelect.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                selectImage();
            }
        });
        ivImage = (ImageView) findViewById(R.id.ivImage);
        btnSubmit = (Button) findViewById(R.id.addShopSubmit);
        btnSubmit.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {


                EditText shopName=(EditText)findViewById(R.id.addShopName);
                EditText shopAddress=(EditText)findViewById(R.id.addShopAddress);
                EditText shopPrimaryContact=(EditText)findViewById(R.id.addShopPrimaryContact);
                EditText shopSecondaryContact=(EditText)findViewById(R.id.addShopSecondaryContact);
                EditText shopDelCharge=(EditText)findViewById(R.id.addShopDelCharge);
                EditText shopMaxTime=(EditText)findViewById(R.id.addShopMaxTime);
                EditText shopMinOrder=(EditText)findViewById(R.id.addShopMinOrder);
                TextView shopStartTime=(TextView)findViewById(R.id.addShopStartTimeLabel);
                TextView shopEndTime=(TextView)findViewById(R.id.addShopEndTimeLabel);
                EditText addShopLatitude=(EditText)findViewById(R.id.addShopLatitude);
                EditText addShopLongitude=(EditText)findViewById(R.id.addShopLongitude);
                Log.e("shopName",shopName.getText().toString()); String sn=shopName.getText().toString();
                Log.e("shopAdd",shopAddress.getText().toString()); String sa=shopAddress.getText().toString();
                Log.e("shopStartTime",shopStartTime.getText().toString()); String sat=shopStartTime.getText().toString();
                Log.e("shopEndTime",shopEndTime.getText().toString()); String set=shopEndTime.getText().toString();
                Log.e("lat",addShopLatitude.getText().toString()); String lat=addShopLatitude.getText().toString();
                Log.e("lon",addShopLongitude.getText().toString()); String lon=addShopLongitude.getText().toString();
                Log.e("shopPrima",shopPrimaryContact.getText().toString()); String spc=shopPrimaryContact.getText().toString();
                Log.e("shopSecondaryContact",shopSecondaryContact.getText().toString()); String ssc=shopSecondaryContact.getText().toString();
                String sdc=shopDelCharge.getText().toString();
                String smt=shopMaxTime.getText().toString();
                String smo=shopMinOrder.getText().toString();
                //SendPostReqAsyncTask sendPostReqAsyncTask = new SendPostReqAsyncTask();
                //sendPostReqAsyncTask.execute(sn,sa,sat,set,lat,lon,spc,ssc,sdc,smt,smo);
                GetDataFromServer dao = new GetDataFromServer();
                dao.postshopdata(sn,sa,sat,set,lat,lon,spc,ssc,sdc,smt,smo);
            }

        });
    }

    @Override
    public void selectedIndices(List<Integer> indices) {

    }
    @Override
    public void selectedStrings(List<String> strings) {
        // Toast.makeText(this, strings.toString(), Toast.LENGTH_LONG).show();
    }
    public void showTimePickerDialog(View v) {
        DialogFragment newFragment = new TimePickerFragment();
        newFragment.show(getFragmentManager(), "timePicker");
    }
    public void showTimePickerDialogend(View v) {
        DialogFragment newFragment = new TimePickerFragmentEnd();
        newFragment.show(getFragmentManager(), "timePicker");
    }

    private void selectImage() {
        final CharSequence[] items = { "Take Photo", "Choose from Library",
                "Cancel" };

        AlertDialog.Builder builder = new AlertDialog.Builder(ShopAdd.this);
        builder.setTitle("Add Photo!");
        builder.setItems(items, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int item) {
                if (items[item].equals("Take Photo")) {
                    Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                    startActivityForResult(intent, REQUEST_CAMERA);
                } else if (items[item].equals("Choose from Library")) {
                    Intent intent = new Intent(
                            Intent.ACTION_PICK,
                            android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                    intent.setType("image/*");
                    startActivityForResult(
                            Intent.createChooser(intent, "Select File"),
                            SELECT_FILE);
                } else if (items[item].equals("Cancel")) {
                    dialog.dismiss();
                }
            }
        });
        builder.show();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode == Activity.RESULT_OK) {
            if (requestCode == SELECT_FILE)
                onSelectFromGalleryResult(data);
            else if (requestCode == REQUEST_CAMERA)
                onCaptureImageResult(data);
        }
    }

    private void onCaptureImageResult(Intent data) {
        Bitmap thumbnail = (Bitmap) data.getExtras().get("data");
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        thumbnail.compress(Bitmap.CompressFormat.JPEG, 90, bytes);

        File destination = new File(Environment.getExternalStorageDirectory(),
                System.currentTimeMillis() + ".jpg");

        FileOutputStream fo;
        try {
            destination.createNewFile();
            fo = new FileOutputStream(destination);
            fo.write(bytes.toByteArray());
            fo.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        ivImage.setImageBitmap(thumbnail);
    }

    @SuppressWarnings("deprecation")
    private void onSelectFromGalleryResult(Intent data) {
        Uri selectedImageUri = data.getData();
        String[] projection = { MediaStore.MediaColumns.DATA };
        Cursor cursor = managedQuery(selectedImageUri, projection, null, null,
                null);
        int column_index = cursor.getColumnIndexOrThrow(MediaStore.MediaColumns.DATA);
        cursor.moveToFirst();

        String selectedImagePath = cursor.getString(column_index);

        Bitmap bm;
        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;
        BitmapFactory.decodeFile(selectedImagePath, options);
        final int REQUIRED_SIZE = 200;
        int scale = 1;
        while (options.outWidth / scale / 2 >= REQUIRED_SIZE
                && options.outHeight / scale / 2 >= REQUIRED_SIZE)
            scale *= 2;
        options.inSampleSize = scale;
        options.inJustDecodeBounds = false;
        bm = BitmapFactory.decodeFile(selectedImagePath, options);

        ivImage.setImageBitmap(bm);
    }

    public void uploadImage(View v){
        try{
            Map<String, String> params = new HashMap<String, String>();

     //       Bitmap file = ((BitmapDrawable)ivImage.getDrawable()).getBitmap();
            //Converting Bitmap to String
     //       String image = getStringImage(file);

            //Getting Image Name
            //String name = editTextName.getText().toString().trim();

            //Creating parameters
            //Map<String,String> params = new Hashtable<>();

            EditText shopName=(EditText)findViewById(R.id.addShopName);
            EditText shopAddress=(EditText)findViewById(R.id.addShopAddress);
            EditText shopPrimaryContact=(EditText)findViewById(R.id.addShopPrimaryContact);
            EditText shopSecondaryContact=(EditText)findViewById(R.id.addShopSecondaryContact);
            EditText shopDelCharge=(EditText)findViewById(R.id.addShopDelCharge);
            EditText shopMaxTime=(EditText)findViewById(R.id.addShopMaxTime);
            EditText shopMinOrder=(EditText)findViewById(R.id.addShopMinOrder);
            TextView shopStartTime=(TextView)findViewById(R.id.addShopStartTimeLabel);
            TextView shopEndTime=(TextView)findViewById(R.id.addShopEndTimeLabel);
            EditText addShopLatitude=(EditText)findViewById(R.id.addShopLatitude);
            EditText addShopLongitude=(EditText)findViewById(R.id.addShopLongitude);
            Log.e("shopName",shopName.getText().toString());
            Log.e("shopAdd",shopAddress.getText().toString());
            Log.e("shopStartTime",shopStartTime.getText().toString());
            Log.e("shopEndTime",shopEndTime.getText().toString());
            Log.e("lat",addShopLatitude.getText().toString());
            Log.e("lon",addShopLongitude.getText().toString());
            Log.e("shopPrima",shopPrimaryContact.getText().toString());
            Log.e("shopSecondaryContact",shopSecondaryContact.getText().toString());


            //Adding parameters
     //       params.put("image1", image);
            params.put("name", "image1");
            params.put("shopName", shopName.getText().toString());
            params.put("shopAddress", shopAddress.getText().toString());
            params.put("shopStartTime", shopStartTime.getText().toString());
            params.put("shopEndTime", shopEndTime.getText().toString());
            params.put("shopPrimaryContact", shopPrimaryContact.getText().toString());
            params.put("shopSecondaryContact", shopSecondaryContact.getText().toString());
            params.put("shopDelCharge", shopDelCharge.getText().toString());
            params.put("shopMaxTime", shopMaxTime.getText().toString());
            params.put("shopMinOrder", shopMinOrder.getText().toString());
            params.put("latitude", addShopLatitude.getText().toString());
            params.put("longitude", addShopLongitude.getText().toString());

            CustomRequest jsObjRequest = new CustomRequest(Request.Method.POST, UPLOAD_URL, params, new Response.Listener<JSONObject>() {

                @Override
                public void onResponse(JSONObject response) {
                    Log.e("Response: ", response.toString());
                    try {

                    } catch (Exception e) {
                        // TODO Auto-generated catch block
                        e.printStackTrace();
                    }

                }
            }, new Response.ErrorListener() {

                @Override
                public void onErrorResponse(VolleyError response) {
                    Log.e("Response: ", response.toString());
                }
            });
            ApplicationController.getInstance().addToRequestQueue(jsObjRequest);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    public String getStringImage(Bitmap bmp){
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        bmp.compress(Bitmap.CompressFormat.JPEG, 100, baos);
        byte[] imageBytes = baos.toByteArray();
        String encodedImage = Base64.encodeToString(imageBytes, Base64.DEFAULT);
        return encodedImage;
    }

    public void sendPostRequest(View v ) {




    }

}