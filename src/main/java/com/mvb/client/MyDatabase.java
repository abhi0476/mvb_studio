package com.mvb.client;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteQueryBuilder;
import android.location.Location;
import android.util.Log;

import com.mvb.client.models.Category;
import com.mvb.client.models.Product;
import com.mvb.client.models.ShopReview;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;

public class MyDatabase extends SQLiteAssetHelper {

	private static final String DATABASE_NAME = "mvbnew-v6.db";
	private static final int DATABASE_VERSION = 6;
	
	public MyDatabase(Context context) {
		super(context, DATABASE_NAME, null, DATABASE_VERSION);	
		
		// you can use an alternate constructor to specify a database location
		// (such as a folder on the sd card)
		// you must ensure that this folder is available and you have permission
		// to write to it
		//super(context, DATABASE_NAME, context.getExternalFilesDir(null).getAbsolutePath(), null, DATABASE_VERSION);

        // call this method to force a database overwrite every time the version number increments:
        //setForcedUpgrade();

		// call this method to force a database overwrite if the version number
		// is below a certain threshold:
		//setForcedUpgrade(2);
	}

	public Cursor getEmployees() {

		SQLiteDatabase db = getReadableDatabase();
		SQLiteQueryBuilder qb = new SQLiteQueryBuilder();

		String [] sqlSelect = {"0 _id", "name"}; 
		String sqlTables = "products";

		qb.setTables(sqlTables);
		Cursor c = qb.query(db, sqlSelect, null, null,
				null, null, null);
		 Log.i("Mydatabase", "inside getEmployees >>");
		c.moveToFirst();
		while (!c.isAfterLast()) {
		   
		    Log.i("Mydatabase", c.getString(1));
		   
		    c.moveToNext();
		}
		return c;

	}
	
	public ArrayList<Shop> getShops(int selectedCategory, Location location) {

		SQLiteDatabase db = getReadableDatabase();
		SQLiteQueryBuilder qb = new SQLiteQueryBuilder();
		
		String [] sqlSelect = {"0 _id", "name","address","latitude","longitude","mobile","anytimedelivery","starttime","endtime","charge","minimum_order","max_distance"}; 
		String sqlTables = "shops";
		ArrayList<Shop> shopList = new ArrayList<Shop>();
		qb.setTables(sqlTables);
		Cursor c;
		String condition="";
		Log.i("selectedCategory", selectedCategory+"");
		double latDist = MyDatabase.round(1.0 / 111.1 * 3.0,6);
		double lonDist;

		
		if(location!=null){
			lonDist = MyDatabase.round(1.0 / Math.abs(111111.0*Math.cos(location.getLatitude())) * 3.0,6);
			//"SELECT * FROM items WHERE latitude BETWEEN latitude - latDist AND latitude + latDist AND longitude BETWEEN longitude - lonDist AND longitude + lonDist  ORDER BY ((location_lat-lat)*(location_lat-lat)) + ((location_lng - lng)*(location_lng - lng)) ASC"
				condition=condition+" shops.latitude BETWEEN shops.latitude - "+latDist+" AND shops.latitude + "+latDist+" and shops.longitude BETWEEN shops.longitude - "+lonDist+" AND shops.longitude + "+lonDist+" ORDER BY ((shops.latitude-"+location.getLatitude()+")*(shops.latitude-"+location.getLatitude()+")) + ((shops.longitude-"+location.getLongitude()+")*(shops.longitude-"+location.getLongitude()+")) ";
		}
		
		if (selectedCategory == 0) {
			if(!condition.equalsIgnoreCase("")){
				condition=" where "+condition;
			}
			String sql="select _id,name,address,latitude,longitude,mobile,anytimedelivery,starttime,endtime,charge,minimum_order,max_distance, (select group_concat(cat.name) from brand_categories as bc, categories as cat where bc.category_id=cat._id and bc.brand_id in( SELECT sb.brand_id FROM shop_brands as sb WHERE sb.shop_id=shops._id )) as catg from shops "+condition;
			Log.i("Mydatabase", sql);
			c = db.rawQuery(sql,null);
		} else {
			if(!condition.equalsIgnoreCase("")){
				condition=" and "+condition;
			}
			c = db.rawQuery(
					"select shops._id as _id ,name,address,latitude,longitude,mobile,anytimedelivery,starttime,endtime,charge,minimum_order,max_distance, (select group_concat(cat.name) from brand_categories as bc, categories as cat where bc.category_id=cat._id  and bc.brand_id in( SELECT sb.brand_id FROM shop_brands as sb WHERE sb.shop_id=shops._id )) as catg from shops ,shop_brands,brand_categories where shops._id=shop_brands.shop_id and shop_brands.brand_id=brand_categories.brand_id  and brand_categories.category_id="+ selectedCategory+" "+condition ,
					null);
		}
		
		 
		c.moveToFirst();
		while (!c.isAfterLast()) {
		   
		    Log.i("Mydatabase", c.getString(1));
		    Shop shop=new Shop();
		    shop.id=c.getInt(0);
		    shop.address=c.getString(2);
		    shop.name=c.getString(1);
		    shop.lat=c.getDouble(3); 
		    shop.lon=c.getDouble(4);
		    shop.mobile=c.getString(5);
		    shop.maxTime="45 Mins";
		    shop.startTime=c.getString(7);
		    shop.endTime=c.getString(8);
		    shop.charge=c.getString(9);
		    shop.minOrder=c.getString(10);
		    shop.maxDistance=c.getString(11);
		    shop.mainCategory=c.getString(12);
		    shopList.add(shop);
		    c.moveToNext();
		}
		return shopList;

	}
	
	public ArrayList<Category> getCategories() {

		SQLiteDatabase db = getReadableDatabase();
		SQLiteQueryBuilder qb = new SQLiteQueryBuilder();
		ArrayList<Category> catgList = new ArrayList<Category>();
//		Cursor c = qb.query(db, sqlSelect, null, null,
//				null, null, null);
		Category catgAll=new Category();
		catgAll.id=0;
		catgAll.name="All";
		catgList.add(catgAll);
		Cursor c = db.rawQuery("select _id,name from categories where enabled=1 order by name asc", null);
		Log.i("Mydatabase", "inside getCategories >>");
		c.moveToFirst();
		while (!c.isAfterLast()) {
		   
		    Log.i("Mydatabase", c.getString(1));
		    Category catg=new Category();
		    catg.id=c.getInt(0);
		    catg.name=c.getString(1);
		    catgList.add(catg);
		    c.moveToNext();
		}
		return catgList;

	}
	
	public ArrayList<Product> getProductsByShopId(int shopId) {

		SQLiteDatabase db = getReadableDatabase();
		SQLiteQueryBuilder qb = new SQLiteQueryBuilder();
		ArrayList<Product> productsList = new ArrayList<Product>();
		
		Cursor c = db.rawQuery("select products._id as id, name,details,category,brand_id,price,discount from products,brand_products where products._id=brand_products.product_id and products.enabled=1 and brand_products.enabled=1 and brand_products.brand_id in (select brand_id from shop_brands where shop_brands.shop_id="+shopId+")", null);
		Log.i("Mydatabase", "inside getProductsByShopId >>");
		c.moveToFirst();
		while (!c.isAfterLast()) {
		   
		    Log.i("Mydatabase", c.getString(1));
		    Product product=new Product();
		    product.id=c.getInt(0);
		    product.name=c.getString(1);
		    product.details=c.getString(2);
		    product.category=c.getInt(3); 
		    product.brand_id=c.getInt(4);
		    product.price=c.getDouble(5);
		    product.discount=c.getDouble(6);
		    productsList.add(product);
		    c.moveToNext();
		}
		return productsList;

	}
	public static double round(double value, int places) {
	    if (places < 0) throw new IllegalArgumentException();

	    BigDecimal bd = new BigDecimal(value);
	    bd = bd.setScale(places, RoundingMode.HALF_UP);
	    return bd.doubleValue();
	}
	
	public void insertIntoReview(int shopId, String review, String deviceId) {

		SQLiteDatabase database = this.getWritableDatabase();
		ContentValues values = new ContentValues();
		values.put("review", review);
		values.put("shop_id", shopId);
		values.put("user_id", deviceId);
		database.insert("shop_reviews", null, values);
		database.close();

	}
	
	public ArrayList<ShopReview> getReviewsByShopId(int shopId) {

		SQLiteDatabase db = getReadableDatabase();
		SQLiteQueryBuilder qb = new SQLiteQueryBuilder();
		ArrayList<ShopReview> reviewList = new ArrayList<ShopReview>();
		
		Cursor c = db.rawQuery("select _id as id, review,shop_id,user_id,created from shop_reviews where shop_id="+shopId+" and enabled=1", null);
		Log.i("Mydatabase", "inside getReviewsByShopId >>");
		c.moveToFirst();
		while (!c.isAfterLast()) {
		   
		    Log.i("Mydatabase", c.getString(1));
		    ShopReview review=new ShopReview();
		    review.id=c.getInt(0);
		    review.review=c.getString(1);
		    review.shopId=c.getInt(2);
		    review.userId=c.getString(3);
		    review.created=c.getString(4);
		    reviewList.add(review);
		    c.moveToNext();
		}
		return reviewList;

	}

}
