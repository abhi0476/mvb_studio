package com.mvb.client;

import android.accounts.Account;
import android.accounts.AccountManager;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.app.Fragment;
import android.app.FragmentManager;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.res.Configuration;
import android.content.res.TypedArray;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.Point;
import android.hardware.SensorManager;
import android.location.Location;
import android.location.LocationManager;
import android.media.Image;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;

import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.util.Log;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.WindowManager;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.GoogleMap.InfoWindowAdapter;
import com.google.android.gms.maps.GoogleMap.OnCameraChangeListener;
import com.google.android.gms.maps.GoogleMap.OnInfoWindowClickListener;
import com.google.android.gms.maps.GoogleMap.OnMapLongClickListener;
import com.google.android.gms.maps.GoogleMap.OnMarkerClickListener;
import com.google.android.gms.maps.GoogleMap.OnMarkerDragListener;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.Polyline;
import com.mvb.client.dao.GetDataFromServer;
import com.mvb.client.library.ActionSlideExpandableListView;
import com.mvb.client.library.MultiSelectionSpinner;
import com.mvb.client.models.Category;
import com.mvb.client.models.NavDrawerItem;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONException;
import org.json.JSONObject;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.InetAddress;
import java.util.ArrayList;
import java.util.List;


public class MainActivity extends AppCompatActivity implements
        android.location.LocationListener, OnMarkerDragListener, MultiSelectionSpinner.OnMultipleItemsSelectedListener{

    String[] array = {"Atta Chakki", "Sweets", "Grocery", "Water", "Fruits", "Vegetables", "Medicines", "Flowers", "Other"};
    ImageView ivImage;
    private DrawerLayout mDrawerLayout;
    private ListView mDrawerList;
    //private ActionBarDrawerToggle mDrawerToggle;

    // nav drawer title
    private CharSequence mDrawerTitle;

    // used to store app title
    private CharSequence mTitle;

    // slide menu items
    private String[] navMenuTitles;
    private TypedArray navMenuIcons;

    private ArrayList<NavDrawerItem> navDrawerItems;
    private NavDrawerListAdapter adapter;



    /** The Sliding list view. */
    private ListView mSlidingListView;

    /** The Main adapter data. */
    // private ArrayList<Category> mMainAdapterData;

    /** The Sliding adapter. */
    // private SlidingAdapter mSlidingAdapter;

    /** The catalog xml parser. */
    // private CatalogXmlParser catalogXmlParser;
    // Progress Dialog
    private ProgressDialog pDialog;

    int REQUEST_CAMERA = 0, SELECT_FILE = 1;
    Button btnSelect;
    private int PICK_IMAGE_REQUEST = 1;
    // Creating JSON Parser object
    JSONParser jParser = new JSONParser();
    private ArrayList<SpinnerNavItem> navSpinner;
    Spinner spinner_nav;
    private MenuItem mSearchAction;
    private boolean isSearchOpened = false;
    private EditText edtSeach;
    boolean isInternetAvialable=false;
    /** The Drawer toggle. */
    private ActionBarDrawerToggle mDrawerToggle;
    ArrayList<String> favShoplist=null;
    /** The Ab search. */
    private View mAbSearch;

    /** The Ab checkout. */
    private View mAbCheckout;

    /** The Ab cart image view. */
    private View mAbCartImageView;

    /** The Ab clear filter. */
    private View mAbClearFilter;

    /** The Ab search et. */
    private EditText mAbSearchEt;
    private ImageView mImageView;
    AutoCompleteTextView autocompleteViewNew;
    /** The Ab cancel search. */
    private View mAbCancelSearch;
    private View mAbClearSearch;
    private View logo;
    private View hideButton;
    private View showButton;
    private View mAbSent;
    private Cursor employees;
    private Cursor shops;
    private MyDatabase db;
    private LinearLayout countLayout;
    ProgressDialog pd ;

    GoogleMap map;
    String name, id, frnd_name, frnd_lat, frnd_longi, frnd_id;
    LatLng my_latlong, frnd_latlong,cameraLatLng;
    TextView tv;
    // private TextToSpeech tts;
    ArrayList<LatLng> directionPoint;
    int size_of_latlong, latlong_index = 0;
    ArrayList<Polyline> polylines;
    String direction = "walking";
    Document doc;
    Location mLastLocation;
    boolean processFlag=true;
    boolean searchFlag=false;
    ActionBarDrawerToggle drawerToggle=null;
    SensorManager sensor_manager;
    // private int result=0;
    MarkerOptions mo, frnd_mo;
    Marker my_marker, frnd_marker;
    boolean first_time_flag = false, update_flag = false;
    String[] numbers = { "Ace", "2", "3", "4", "5", "6", "7", "8", "9", "10",
            "Jack", "Queen", "King" };
    ArrayList<String> QuestionForSliderMenu = new ArrayList<String>();
    public ArrayList<Shop> shopList = new ArrayList<Shop>();
    // The minimum distance to change Updates in meters
    private static final long MIN_DISTANCE_CHANGE_FOR_UPDATES = 10; // 10 meters
    private static final long DEFAULT_ZOOM_LEVEL = 15;
    private static final long SHOP_ZOOM_LEVEL = 16;

    // The minimum time between updates in milliseconds
    private static final long MIN_TIME_BW_UPDATES = 1000 * 60 * 1; // 1 minute

    // Declaring a Location Manager
    protected LocationManager locationManager;

    protected Toolbar mToolbar;
    // flag for GPS status
    boolean isGPSEnabled = false;
    // flag for network status
    boolean isNetworkEnabled = false;

    // flag for GPS status
    boolean canGetLocation = false;

    Location location = null; // location
    public static Location lastLocation=null;
    double latitude; // latitude
    double longitude; // longitude
    Marker pinMarker;
    boolean isRefresh=false;
    public int selectedCategory=0;
    public boolean isfirst=true;
    public String selectedCategoryName="";
    ShopListAdapter mAdapter;
    AlertDialog dialog;
    AlertDialog.Builder b;
    int mSelected = -1;
    Marker lastOpenned = null;
    public boolean hasPropmtedForGPS = false;
    public static String lastSearchedString=null;
    public ArrayList<Marker> markers = new ArrayList<Marker>();
    //ActionSlideExpandableListView listView;
    ListView listView;
    public static String emailId="";
    public Bundle savedInstance=null;
    Dialog myDialog=null;
    @SuppressLint("NewApi")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.welcome);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        db = new MyDatabase(this);
        emailId=getEmail();
        loadActivity();
        pd= new ProgressDialog(MainActivity.this);
        this.savedInstance=savedInstanceState;
    }

    public String getEmail(){
        Account[] accounts = AccountManager.get(this).getAccounts();
        Log.e("", "Size: " + accounts.length);
        for (Account account : accounts) {

            String possibleEmail = account.name;
            String type = account.type;

            if (type.equals("com.google")) {
                return possibleEmail;
            }
        }
        return "";
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        hasPropmtedForGPS=true;
        if (resultCode == Activity.RESULT_OK) {
            if (requestCode == SELECT_FILE)
                onSelectFromGalleryResult(data);
            else if (requestCode == REQUEST_CAMERA)
                onCaptureImageResult(data);
        }
        loadActivity();
    }

    public void loadActivity() {

        initToolbar();
        addItemsToSpinner();
        try {
            getLocation();
            processFlag=false;
            b =new AlertDialog.Builder(MainActivity.this);
            b.setTitle("No Connection");
            b.setMessage("Cannot connect to the internet!");


            b.setPositiveButton("Retry", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.dismiss();
                    getLocation();
                    new CheckInternet().execute();
                }
            });
            b.setNegativeButton("Exit",new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog,int id) {
                    // if this button is clicked, just close
                    // the dialog box and do nothing
                    dialog.dismiss();
                    android.os.Process.killProcess(android.os.Process.myPid());
                    System.exit(1);
                }
            });
            new CheckInternet().execute();
            //LatLng lc=new LatLng(location.getLatitude(),location.getLongitude());
            //new LoadShops().execute(lc);
//			shopList = db.getShops(selectedCategory,location);
//			setUpMap();
//			setUpExpandableList();
//			setMarkersOnMap();
//			setMarkersListener();
        } catch (Exception e) {
            e.getStackTrace();
        }
    }

    public void refreshActivity(){
        //shopList.clear();
        try {
            LatLng lc=new LatLng(location.getLatitude(),location.getLongitude());
            if(processFlag){
                isRefresh=true;
                new CheckRefreshInternet().execute();
                //new CheckInternet().execute();
            }
//			if(processFlag && isInternetAvialable){
//				new RefreshShops().execute(lc);
//			}
//			ArrayList<Shop> newshopList = db.getShops(selectedCategory,location);
//			shopList.clear();
//			shopList=newshopList;
//			map.clear();
//			setMarkersOnMap();
//			mAdapter.swapItems(newshopList,markers);


        } catch (Exception e) {
            e.getStackTrace();
        }
    }

    public void initToolbar() {
        mToolbar = (Toolbar) findViewById(R.id.toolbar);
        spinner_nav = (Spinner) findViewById(R.id.spinner_nav);
        mAbSearch = (ImageView) findViewById(R.id.ab_custom_search);
        mImageView= (ImageView) findViewById(R.id.addShopBtn);
        mAbClearSearch = (TextView)findViewById(R.id.ab_custom_cancel_search);
        mAbCancelSearch = (ImageView)findViewById(R.id.back);
        logo = (ImageView)findViewById(R.id.logo);
        hideButton = (ImageView)findViewById(R.id.hideButton);
        showButton = (ImageView)findViewById(R.id.showButton);
        countLayout = (LinearLayout) findViewById(R.id.listCountLayout);

        mAbSearch.setVisibility(View.VISIBLE);
        AutoCompleteTextView autocompleteView = (AutoCompleteTextView) findViewById(R.id.autocomplete);
        autocompleteViewNew=autocompleteView;
        autocompleteView.setAdapter(new PlacesAutoCompleteAdapter(getApplicationContext(), R.layout.autocomplete_list_item));
        autocompleteView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                // Get data associated with the specified position
                // in the list (AdapterView)
                String description = (String) parent.getItemAtPosition(position);
                if (processFlag) {
                    new CheckInternetWithoutDialog().execute(description);
                }
                //new ChangePin().execute(description);
                //Toast.makeText(getApplicationContext(), description, Toast.LENGTH_SHORT).show();
            }
        });


        mAbSearch.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                showSearchAb();
                autocompleteViewNew.requestFocus();
                InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.toggleSoftInput(InputMethodManager.SHOW_FORCED, 0);



                // imm.showSoftInput(autocompleteViewNew, InputMethodManager.SHOW_FORCED);

            }
        });

        mImageView.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                Log.e("click add","clickkk");
               // Intent myIntent = new Intent(MainActivity.this, ShopAdd.class);
               // myIntent.putExtra("key", value); //Optional parameters
               // MainActivity.this.startActivity(myIntent);

                 myDialog = new Dialog(MainActivity.this);
                myDialog.setContentView(R.layout.shop_add);
                myDialog.setCancelable(true);
                myDialog.setTitle("Add Shop");

                WindowManager manager = (WindowManager) getSystemService(Activity.WINDOW_SERVICE);
                int width, height;
                WindowManager.LayoutParams params;


                width = manager.getDefaultDisplay().getWidth();
                height = manager.getDefaultDisplay().getHeight();

                WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
                lp.copyFrom(myDialog.getWindow().getAttributes());
                lp.width = width;
               // lp.height = height;
                myDialog.getWindow().setAttributes(lp);

                final MultiSelectionSpinner multiSelectionSpinner = (MultiSelectionSpinner) myDialog.findViewById(R.id.catgSpinner);
                multiSelectionSpinner.setItems(array);
                multiSelectionSpinner.setSelection(new int[]{2});
                multiSelectionSpinner.setListener(MainActivity.this);
                btnSelect = (Button) myDialog.findViewById(R.id.btnSelectPhoto);
                btnSelect.setOnClickListener(new View.OnClickListener() {

                    @Override
                    public void onClick(View v) {
                        selectImage();
                    }
                });
                ivImage = (ImageView) myDialog.findViewById(R.id.ivImage);
                Button addShopSubmit=(Button)myDialog.findViewById(R.id.addShopSubmit);
                final EditText shopName=(EditText)myDialog.findViewById(R.id.addShopName);
                final EditText shopAddress=(EditText)myDialog.findViewById(R.id.addShopAddress);
                final EditText shopPrimaryContact=(EditText)myDialog.findViewById(R.id.addShopPrimaryContact);
                final EditText shopSecondaryContact=(EditText)myDialog.findViewById(R.id.addShopSecondaryContact);
                final EditText shopDelCharge=(EditText)myDialog.findViewById(R.id.addShopDelCharge);
                final EditText shopMaxTime=(EditText)myDialog.findViewById(R.id.addShopMaxTime);
                final EditText shopMinOrder=(EditText)myDialog.findViewById(R.id.addShopMinOrder);
                final TextView shopStartTime=(TextView)myDialog.findViewById(R.id.addShopStartTimeLabel);
                final TextView shopEndTime=(TextView)myDialog.findViewById(R.id.addShopEndTimeLabel);
                final EditText addShopLatitude=(EditText)myDialog.findViewById(R.id.addShopLatitude);
                final EditText addShopLongitude=(EditText)myDialog.findViewById(R.id.addShopLongitude);
                addShopLatitude.setText(location.getLatitude()+"");
                addShopLongitude.setText(location.getLongitude()+"");
                myDialog.show();

                addShopSubmit.setOnClickListener(new OnClickListener()
                {

                    @Override
                    public void onClick(View v)
                    {

                        //your login calculation goes here
                        Log.e("shopName",shopName.getText().toString()); String sn=shopName.getText().toString();
                        Log.e("shopAdd",shopAddress.getText().toString()); String sa=shopAddress.getText().toString();
                        Log.e("shopCatg",multiSelectionSpinner.getSelectedIndices().toString()); String sc=multiSelectionSpinner.getSelectedIndices().toString();
                        Log.e("shopStartTime",shopStartTime.getText().toString()); String sat=shopStartTime.getText().toString();
                        Log.e("shopEndTime",shopEndTime.getText().toString()); String set=shopEndTime.getText().toString();
                        Log.e("lat",addShopLatitude.getText().toString()); String lat=addShopLatitude.getText().toString();
                        Log.e("lon",addShopLongitude.getText().toString()); String lon=addShopLongitude.getText().toString();
                        Log.e("shopPrima",shopPrimaryContact.getText().toString()); String spc=shopPrimaryContact.getText().toString();
                        Log.e("shopSecondaryContact",shopSecondaryContact.getText().toString()); String ssc=shopSecondaryContact.getText().toString();
                        String sdc=shopDelCharge.getText().toString();
                        String smt=shopMaxTime.getText().toString();
                        String smo=shopMinOrder.getText().toString();
                        //GetDataFromServer dao = new GetDataFromServer();
                        //dao.postshopdata(sn,sa,sat,set,lat,lon,spc,ssc,sdc,smt,smo);
                        new BackgroundTask().execute(sn, sa, sat, set,lat,lon,spc,ssc,sdc,smt,smo,sc);
                    }
                });


            }
        });

        mAbCancelSearch.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {

                showCommonAb();

                InputMethodManager imm = (InputMethodManager) getSystemService(INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(autocompleteViewNew.getWindowToken(), 0);
                autocompleteViewNew.setText("");
            }
        });

        mAbClearSearch.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {

               // showCommonAb();
                //setUpAdapter();

                //InputMethodManager imm = (InputMethodManager) getSystemService(INPUT_METHOD_SERVICE);
                //imm.hideSoftInputFromWindow(autocompleteViewNew.getWindowToken(), 0);
                autocompleteViewNew.setText("");
            }
        });
        hideButton.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                LinearLayout listLayout = (LinearLayout) findViewById(R.id.listLayout);
                listLayout.setVisibility(View.GONE);
                countLayout.setVisibility(View.VISIBLE);
                hideButton.setVisibility(View.GONE);
                showButton.setVisibility(View.VISIBLE);
            }
        });

        showButton.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                LinearLayout listLayout = (LinearLayout) findViewById(R.id.listLayout);
                listLayout.setVisibility(View.VISIBLE);
                countLayout.setVisibility(View.GONE);
                hideButton.setVisibility(View.VISIBLE);
                showButton.setVisibility(View.GONE);
            }
        });

        countLayout.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                LinearLayout listLayout = (LinearLayout) findViewById(R.id.listLayout);
                listLayout.setVisibility(View.VISIBLE);
                countLayout.setVisibility(View.GONE);
                hideButton.setVisibility(View.VISIBLE);
                showButton.setVisibility(View.GONE);
            }
        });





        if (mToolbar != null) {

            mTitle = mDrawerTitle = getTitle();



            DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawerLayout);
             drawerToggle = new ActionBarDrawerToggle(
                    this, drawer, mToolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
            drawer.setDrawerListener(drawerToggle);
            drawerToggle.syncState();

            NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
            navigationView.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {
                @Override
                public boolean onNavigationItemSelected(MenuItem menuItem) {
                    int id = menuItem.getItemId();
                    Log.e("item selected",id+"");
                    if (id == R.id.mynotesicon) {
                        Fragment fragment =new NoteFragment();

                    if (fragment != null) {
                        FragmentManager fragmentManager = getFragmentManager();
                        fragmentManager.beginTransaction().replace(R.id.frame_container, fragment).addToBackStack("mynotes").commit();
                    }
                        // Handle the camera action
                    } else if (id == R.id.myfavicon) {


                    } else if (id == R.id.calllogicon) {

                    } else if (id == R.id.addshopicon) {
                        mImageView.performClick();

                    }

                    DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawerLayout);
                    drawer.closeDrawer(GravityCompat.START);
                    //return true;
                    return false;
                }
            });

            if (this.savedInstance == null) {
                // on first time display view for first nav item
                displayView(0);
            }


            if (mToolbar != null) {
                //Log.i("not null", "not null");
                setSupportActionBar(mToolbar);
                getSupportActionBar().setTitle("");
            }
            //getSupportActionBar().setIcon(R.drawable.logo_small);
            //mToolbar.setLogo(R.drawable.logo_small);
            //getSupportActionBar().setHomeButtonEnabled(true);
            //getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        }
    }

    public void setUpMap() {
        map = ((MapFragment) getFragmentManager().findFragmentById(R.id.map))
                .getMap();


        if (map == null){
            Toast.makeText(getApplicationContext(),
                    "Sorry! unable to create Map.", Toast.LENGTH_SHORT).show();
        }
        else {
            map.setMyLocationEnabled(true);
            map.getUiSettings().setZoomControlsEnabled(true);
            map.getUiSettings().setMyLocationButtonEnabled(true);
            map.getUiSettings().setCompassEnabled(true); // false to
            map.getUiSettings().setMapToolbarEnabled(true);
            map.setOnMarkerDragListener(this);
			
				/**/map.setOnCameraChangeListener(new OnCameraChangeListener() {
                @Override
                public void onCameraChange(CameraPosition position) {

                    LatLngBounds bounds = map.getProjection().getVisibleRegion().latLngBounds;
                    if(!searchFlag){
                    if (CalculationByDistance(new LatLng(location.getLatitude(), location.getLongitude()), new LatLng(bounds.getCenter().latitude, bounds.getCenter().longitude)) > 5) {
                        location.setLatitude(bounds.getCenter().latitude);
                        location.setLongitude(bounds.getCenter().longitude);
                        lastLocation=location;
                        if(isfirst){
                            isfirst=false;
                        }else{
                            refreshActivity();
                        }

                    }}
                    searchFlag=false;

                }
            });

            if(location!=null){

                Log.e("animateCamera", location.getLatitude()+" "+location.getLongitude());
                map.animateCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(location.getLatitude(),
                        location.getLongitude()),DEFAULT_ZOOM_LEVEL));

                //map.animateCamera(CameraUpdateFactory.zoomTo(15));
            }
            map.setOnMapLongClickListener(new OnMapLongClickListener() {

                @Override
                public void onMapLongClick(LatLng latLng) {
	            	/*MarkerOptions pinMo = new MarkerOptions()
	    			.position(new LatLng(location.getLatitude(), location.getLongitude())).title("Pin Location");
	                pinMo.icon(BitmapDescriptorFactory
	    					.fromResource(R.drawable.long_pin_icon));
	                location.setLatitude(latLng.latitude);
	 	           location.setLongitude(latLng.longitude);
	 	           refreshActivity();
	 	           if(pinMarker!=null){
	 		           	pinMarker.remove();
	 		           }
	 		           pinMarker=map.addMarker(pinMo);
	 		           pinMarker.setDraggable(true); */




                    map.animateCamera(CameraUpdateFactory.newLatLngZoom(latLng,DEFAULT_ZOOM_LEVEL));

                }
            });
            map.setOnMapClickListener(new GoogleMap.OnMapClickListener() {

                @Override
                public void onMapClick(LatLng latLng) {
                    showCommonAb();

                    InputMethodManager imm = (InputMethodManager) getSystemService(INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(autocompleteViewNew.getWindowToken(), 0);
                    autocompleteViewNew.setText("");
                }
            });
        }
    }
    public static  boolean markerClick=false;
    public static String shopNameClicked = null;

    public void setMarkersListener() {

        map.setOnMarkerClickListener(new OnMarkerClickListener() {

            @Override
            public boolean onMarkerClick(Marker marker) {

                if (lastOpenned != null) {
                    // Close the info window
                    lastOpenned.hideInfoWindow();

                    // Is the marker the same marker that was already open
                    if (lastOpenned.equals(marker)) {
                        // Nullify the lastOpenned object
                        lastOpenned = null;
                        // Return so that the info window isn't openned again
                        return true;
                    }
                }
                for (int i = 0; i < markers.size(); i++) {
                    if (lastOpenned != null && markers.get(i).equals(lastOpenned)) {
                        lastOpenned.hideInfoWindow();
                    }

                    if (markers.get(i).equals(marker)) {

                        Log.e("Marker Matched", "Marker");
                        listView.requestFocusFromTouch();
                        listView.setSelection(i);
                        shopNameClicked = marker.getTitle();
//						if(mSelected!=-1){
//						listView.getAdapter().revertColor(getApplicationContext(), mSelected, null, listView);}
//						listView.getAdapter().changeColor(getApplicationContext(),i, null, listView);
//						listView.getAdapter().notifyDataSetChanged();
                        mSelected = i;
                        lastOpenned = marker;
                        markerClick = true;
                        //LinearLayout layout = (LinearLayout) listView.getSelectedView().findViewById(R.id.list_item_select);
                        //layout.setBackgroundColor(Color.CYAN);
                        //listView.getChildAt(i).setBackgroundColor(getResources().getColor(R.color.list_dark_green));
                        //listView.getAdapter().getView(i, null, listView).setBackgroundColor(getResources().getColor(R.color.list_dark_green));
                        //listView.getAdapter().mSelected=i;
                        //listView.getAdapter().notifyDataSetChanged();
                        //
//			            if (mSelected != -1 && mSelected != i){
//			            	listView.getChildAt(mSelected).setBackgroundColor(getResources().getColor(R.color.list_dark_green));
//			            }
//			
//			            mSelected = i; 
                        //listView.performItemClick(listView.getAdapter().getView(i, null, null), i, i);
                        //listView.getAdapter().getView(i, null, listView).performClick();
                        //listView.getAdapter().enableFor(listView.getAdapter().getView(i, null, listView),i);
                        //listView.getAdapter().clickMarker(listView.getAdapter().getExpandableView(listView.getAdapter().getView(i, null, listView)),i);
                        //listView.getAdapter().getExpandToggleButton(listView.getAdapter().getView(i, null, listView)).performClick();
                        //listView.getAdapter().notifiyExpandCollapseListener(0,listView.getAdapter().getView(i, null, null),i );
                        marker.showInfoWindow();
                    }
                }


                return false;
            }
        });

        listView.setOnScrollListener(new AbsListView.OnScrollListener() {

            @Override
            public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
                //To change body of implemented methods use File | Settings | File Templates.
                if (markerClick) {
                    // LinearLayout layout = (LinearLayout) listView.getChildAt(0).findViewById(R.id.list_item_select);
                    // layout.setBackgroundColor(Color.parseColor("#DCF8C6"));
                    // TextView shop = (TextView) listView.getChildAt(0).findViewById(R.id.shopName);
                    // shopNameClicked = shop.getText().toString();


                    int tot = listView.getLastVisiblePosition() - listView.getFirstVisiblePosition();
                    for (int k = 0; k < tot + 1; k++) {
                        if (listView.getChildAt(k) == null) {
                            break;
                        }
                        LinearLayout layout1 = (LinearLayout) listView.getChildAt(k).findViewById(R.id.list_item_select);
                        TextView shop1 = (TextView) listView.getChildAt(k).findViewById(R.id.shopName);
                        //layout1.setBackgroundColor(Color.CYAN);
                        if (!shopNameClicked.equals(shop1.getText().toString()))
                            layout1.setBackgroundColor(Color.parseColor("#FFFDFF"));
                        else {
                            layout1.setBackgroundColor(Color.parseColor("#E8F1F0"));
                        }
                    }
                    markerClick = false;
                } else {
                    int tot = listView.getLastVisiblePosition() - listView.getFirstVisiblePosition();
                    for (int k = 0; k < tot + 1; k++) {
                        if (listView.getChildAt(k) == null) {
                            break;
                        }
                        LinearLayout layout1 = (LinearLayout) listView.getChildAt(k).findViewById(R.id.list_item_select);
                        TextView shop1 = (TextView) listView.getChildAt(k).findViewById(R.id.shopName);
                        if (shopNameClicked != null && !shopNameClicked.equals(shop1.getText().toString())) {
                            layout1.setBackgroundColor(Color.parseColor("#FFFDFF"));
                        } else if (shopNameClicked != null) {
                            layout1.setBackgroundColor(Color.parseColor("#E8F1F0"));
                        }

                    }
                }
            }

            @Override
            public void onScrollStateChanged(AbsListView view, int firstVisibleItem) {

                //  Toast.makeText(MainActivity.this, "Scroll to Top ", Toast.LENGTH_SHORT).show();
            }
        });

        map.setOnInfoWindowClickListener(new OnInfoWindowClickListener() {
            @Override
            public void onInfoWindowClick(Marker marker) {
                String snippet = marker.getSnippet();
                if (snippet != null) {
                    if (!snippet.equalsIgnoreCase("") && snippet.contains(":")) {
                        String[] snippetParts = marker.getSnippet().split(":");
                        String pp = snippetParts[1];
                        String[] parts = pp.split(",");
                        Log.e("Marker Matched", parts[0]);
                        Intent callIntent = new Intent(Intent.ACTION_CALL);
                        callIntent.setData(Uri.parse("tel:" + parts[0]));
                        startActivity(callIntent);
                    }
                }
            }
        });
    }


    private void buildAlertMessageNoGps() {
        final AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage("Your GPS seems to be disabled, do you want to enable it?")
                .setCancelable(false)
                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    public void onClick(final DialogInterface dialog, final int id) {
                        startActivityForResult(new Intent(android.provider.Settings.ACTION_LOCATION_SOURCE_SETTINGS),1);
                    }
                })
                .setNegativeButton("No", new DialogInterface.OnClickListener() {
                    public void onClick(final DialogInterface dialog, final int id) {
                        dialog.cancel();
                    }
                });
        final AlertDialog alert = builder.create();
        alert.show();

    }

    public void getLocation(){
        locationManager = (LocationManager) this.getApplicationContext().getSystemService(LOCATION_SERVICE);
        // getting GPS status
        isGPSEnabled = locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);
        // getting network status
        isNetworkEnabled = locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER);
        boolean flagLocFound=false;
        if(!isGPSEnabled && !hasPropmtedForGPS){
            buildAlertMessageNoGps();

        }
        if (!isGPSEnabled && !isNetworkEnabled) {
            // no network provider is enabled
            //System.out.println("GPSTracker.getLocation and no network provider is enabled");
            location=locationManager.getLastKnownLocation(LocationManager.PASSIVE_PROVIDER);
            if (location != null) {
                latitude = location.getLatitude();
                longitude = location.getLongitude();

                Log.e("GPS Enabled", "PASSIVE_PROVIDER");
            }
        } else {
            // this.canGetLocation = true;
            // First get location from Network Provider
            if (isNetworkEnabled) {
                locationManager.requestLocationUpdates(
                        LocationManager.NETWORK_PROVIDER, MIN_TIME_BW_UPDATES,
                        MIN_DISTANCE_CHANGE_FOR_UPDATES, this);
                Log.d("Network", "Network");
                if (locationManager != null) {
                    location = locationManager
                            .getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
                    if (location != null) {

                        latitude = location.getLatitude();
                        longitude = location.getLongitude();

                        Log.e("NETWORK_PROVIDER", latitude+" "+longitude);
                        flagLocFound=true;
                    }
                }
            }
            // if GPS Enabled get lat/long using GPS Services
            if (isGPSEnabled) {
                if (location == null) {
//					locationManager.requestLocationUpdates(
//							LocationManager.GPS_PROVIDER, MIN_TIME_BW_UPDATES,
//							MIN_DISTANCE_CHANGE_FOR_UPDATES, this);
                    Log.d("GPS Enabled", "GPS Enabled");
                    if (locationManager != null) {
                        location = locationManager
                                .getLastKnownLocation(LocationManager.GPS_PROVIDER);
                        if (location != null) {
                            Log.e("GPS Enabled", "GPS_PROVIDER");
                            latitude = location.getLatitude();
                            longitude = location.getLongitude();

                            flagLocFound=true;
                        }
                    }
                }
            }
            if(!flagLocFound){
                location=locationManager.getLastKnownLocation(LocationManager.PASSIVE_PROVIDER);
                if (location != null) {
                    Log.e("GPS Enabled", "PASSIVE_PROVIDER");
                    //Log.e("PASSIVE_PROVIDER animateCamera", location.getLatitude()+" "+location.getLongitude());
                    latitude = location.getLatitude();
                    longitude = location.getLongitude();
                }
            }

        }
        if (location == null){
            latitude = 28.632794;
            longitude = 77.219621;
            location=new Location("");
            location.setLatitude(latitude);
            location.setLongitude(longitude);
        }
        lastLocation=location;
    }
    public void setMarkersOnMap() {
        markers.clear();

        map.setMyLocationEnabled(true);

        for (int i = 0; i < shopList.size(); i++) {
            boolean favflag=false;
            Shop sh = shopList.get(i);
            if(favShoplist!=null) {
                for(int j=0;j<favShoplist.size();j++){
                    if(favShoplist.get(j).equalsIgnoreCase(sh.getId()+"")){
                        favflag=true;
                        break;
                    }
                }
            }


            MarkerOptions mo = new MarkerOptions()
                    .position(new LatLng(sh.lat, sh.lon)).title(sh.name)
                    .snippet("" + sh.mainCategory + ": " + sh.mobile);
            if(favflag){
                mo.icon(BitmapDescriptorFactory
                        .fromResource(R.drawable.locpin_small_fav));
            }else{
                mo.icon(BitmapDescriptorFactory
                        .fromResource(R.drawable.locpin_small));
            }

          /*  if (sh.getMainCategory() == null
                    || sh.getMainCategory().equalsIgnoreCase("")) {
                mo.icon(BitmapDescriptorFactory
                        .fromResource(R.drawable.atta_pin));
            } else {
                for (String retval : sh.getMainCategory().split(",")) {
                    String category = retval.toLowerCase();
                    if (retval.toLowerCase().equals("grocery")) {
                        mo.icon(BitmapDescriptorFactory
                                .fromResource(R.drawable.grocery_pin));
                    } else if (retval.toLowerCase().equals("bakery")) {
                        mo.icon(BitmapDescriptorFactory
                                .fromResource(R.drawable.bakery_pin));
                    } else if (retval.toLowerCase().equals("flour")) {
                        mo.icon(BitmapDescriptorFactory
                                .fromResource(R.drawable.atta_pin));
                    } else if (retval.toLowerCase().equals("dairy")) {
                        mo.icon(BitmapDescriptorFactory
                                .fromResource(R.drawable.dairy_pin));
                    } else if (retval.toLowerCase().equals("flowers")) {
                        mo.icon(BitmapDescriptorFactory
                                .fromResource(R.drawable.fruits_pin));
                    } else if (retval.toLowerCase().equals("fruits")) {
                        mo.icon(BitmapDescriptorFactory
                                .fromResource(R.drawable.fruits_pin));
                    } else if (retval.toLowerCase().equals("sweets")) {
                        mo.icon(BitmapDescriptorFactory
                                .fromResource(R.drawable.sweets_pin));
                    } else if (retval.toLowerCase().equals("vegetables")) {
                        mo.icon(BitmapDescriptorFactory
                                .fromResource(R.drawable.vegetables_pin));
                    } else if (retval.toLowerCase().equals("water")) {
                        mo.icon(BitmapDescriptorFactory
                                .fromResource(R.drawable.water_pin));
                    } else {
                        mo.icon(BitmapDescriptorFactory
                                .fromResource(R.drawable.grocery_pin));
                    }
                }
            }
            */
            markers.add(map.addMarker(mo));



            // Setting a custom info window adapter for the google map
            map.setInfoWindowAdapter(new InfoWindowAdapter() {

                // Use default InfoWindow frame
                @Override
                public View getInfoWindow(Marker marker)   {
                    return null;
                }

                // Defines the contents of the InfoWindow
                @Override
                public View getInfoContents(Marker marker) {

                    // Getting view from the layout file info_window_layout
                    View v = getLayoutInflater().inflate(R.layout.pinpopup, null);

                    TextView popuptitle = (TextView)  v.findViewById(R.id.popuptitle);
                    popuptitle.setText(marker.getTitle());
                    popuptitle.setTextColor(Color.BLACK);
                    String snippet=marker.getSnippet();
                    if(snippet!=null){
                        if(!snippet.equalsIgnoreCase("")  && snippet.contains(":")){
                            String[] snippetParts=marker.getSnippet().split(":");
                            TextView popupsnippet = (TextView)  v.findViewById(R.id.popupsnippet);
                            popupsnippet.setText(snippetParts[0]);
                            popupsnippet.setTextColor(Color.BLACK);
                        }else{
                            TextView popupsnippet = (TextView)  v.findViewById(R.id.popupsnippet);
                            popupsnippet.setText(snippet);
                            popupsnippet.setTextColor(Color.BLACK);
                            ImageView popupimage=(ImageView) v.findViewById(R.id.popupcall);
                            popupimage.setVisibility(View.GONE);
                        }
                    }
		                /* final String pp=snippetParts[1];
		                ImageView popupcall = (ImageView)  v.findViewById(R.id.popupcall);   
		                popupcall.setOnClickListener(new OnClickListener() {

		        			@Override
		        			public void onClick(View v) {
		        				String[] parts = pp.split(",");
		        				   Intent callIntent = new Intent(Intent.ACTION_CALL);
		           				callIntent.setData(Uri.parse("tel:"+parts[0]));
		           				getApplicationContext().startActivity(callIntent);

		        			}
		        		});

		        		OnInfoWindowElemTouchListener infoButtonListener=new OnInfoWindowElemTouchListener(popupcall,
				                getResources().getDrawable(R.drawable.call_btn_small),
				                getResources().getDrawable(R.drawable.call_btn_small))
				        {
				            @Override
				            protected void onClickConfirmed(View v, Marker marker) {
				                // Here we can perform some action triggered after clicking the button
				                Toast.makeText(MainActivity.this, marker.getTitle() + "'s button clicked!", Toast.LENGTH_SHORT).show();
				                String[] parts = pp.split(",");
		        				   Intent callIntent = new Intent(Intent.ACTION_CALL);
		           				callIntent.setData(Uri.parse("tel:"+parts[0]));
		           				getApplicationContext().startActivity(callIntent);
				            
				            }
				        };
				        popupcall.setOnTouchListener(infoButtonListener);
		                infoButtonListener.setMarker(marker);
		                mapWrapperLayout.setMarkerWithInfoWindow(marker, v);*/
                    return v;
                }

            });
			
			/*MarkerOptions pinMo = new MarkerOptions()
			.position(new LatLng(location.getLatitude(), location.getLongitude())).title("Pin Location");
            pinMo.icon(BitmapDescriptorFactory
					.fromResource(R.drawable.long_pin_icon));
			pinMarker=map.addMarker(pinMo);
			pinMarker.setDraggable(true);*/

        }
    }

    public void setUpExpandableList() {
         listView = (ListView) findViewById(R.id.list);
        //listView = (ActionSlideExpandableListView) this.findViewById(R.id.list);
        listView.setChoiceMode(ListView.CHOICE_MODE_SINGLE);
        listView.setItemsCanFocus(true);
        TextView textView = (TextView) findViewById(R.id.textView);
        textView.setText(shopList.size() + " Shops");
        mAdapter = new ShopListAdapter(this, shopList,markers,map,favShoplist);

        //listView.setAdapter(mAdapter,markers,map);
        listView.setAdapter(mAdapter);


      /*  listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                Shop item = (Shop) parent.getItemAtPosition(position);
                Log.e("ItemClikc",item.getName());
                map.animateCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(item.getLat(), item.getLon()), 17));
            }
        });
*/
        // listen for events in the two buttons for every list item.
        // the 'position' var will tell which list item is clicked

      /*  listView.setItemActionListener(
                new ActionSlideExpandableListView.OnActionClickListener() {


                    @Override
                    public void onClick(View listView, View buttonview,
                                        int position) {


                        String actionName = "";
//						if (buttonview.getId() == R.id.buttonA) {
//							actionName = "buttonA";
//						} else {
//							actionName = "ButtonB";
//						}

                    }

                    // note that we also add 1 or more ids to the
                    // setItemActionListener
                    // this is needed in order for the listview to
                    // discover the buttons
                }, R.id.expandable_toggle_button); */



    }

    @Override
    public void onLocationChanged(Location locationnew) {
        // TextView locationTv = (TextView) findViewById(R.id.latlongLocation);
        double latitudenew = locationnew.getLatitude();
        double longitudenew = locationnew.getLongitude();
        if(location==null){
            location=new Location("");
            location.setLatitude(latitudenew);
            location.setLongitude(longitudenew);
        }
        //LatLng latLng = new LatLng(latitude, longitude);
        //map.addMarker(new MarkerOptions().position(latLng));
        //map.moveCamera(CameraUpdateFactory.newLatLng(latLng));
        //map.animateCamera(CameraUpdateFactory.zoomTo(16));
        // locationTv.setText("Latitude:" + latitude + ", Longitude:" +
        // longitude);
    }

    @Override
    public void onProviderDisabled(String provider) {
        // TODO Auto-generated method stub
    }

    @Override
    public void onProviderEnabled(String provider) {
        // TODO Auto-generated method stub
    }


    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {
        // TODO Auto-generated method stub
    }

    private boolean isGooglePlayServicesAvailable() {
        int status = GooglePlayServicesUtil.isGooglePlayServicesAvailable(this);
        if (ConnectionResult.SUCCESS == status) {
            return true;
        } else {
            GooglePlayServicesUtil.getErrorDialog(status, this, 0).show();
            return false;
        }
    }

    /**

     * Launching new activity
     * */
    private void LocationFound() {
        // Intent i = new Intent(MainActivity.this, LocationFound.class);
        // startActivity(i);
    }

	/*public class Request_Update extends AsyncTask<Location, Void, Location> {
		@Override
		protected void onPreExecute() {
			// Toast.makeText(getApplicationContext(), "onPreExecute()!",
			// Toast.LENGTH_SHORT).show();
		}

		@Override
		protected Location doInBackground(Location... location) {
			// TODO Auto-generated method stub

			String url = "http://maps.googleapis.com/maps/api/directions/xml?"
					+ "origin=" + location[0].getLatitude() + ","
					+ location[0].getLongitude() + "&destination=" + frnd_lat
					+ "," + frnd_longi + "&sensor=false&units=metric&mode="
					+ direction;

			try {
				HttpClient httpClient = new DefaultHttpClient();
				HttpContext localContext = new BasicHttpContext();
				HttpPost httpPost = new HttpPost(url);
				HttpResponse response = httpClient.execute(httpPost,
						localContext);
				InputStream in = response.getEntity().getContent();
				DocumentBuilder builder = DocumentBuilderFactory.newInstance()
						.newDocumentBuilder();
				doc = builder.parse(in);
			} catch (Exception e) {
			}

			return location[0];
		} 

		@Override
		protected void onPostExecute(Location location) {
			if (doc != null) {
				directionPoint = getDirection(doc);
				int ii = 0;
				size_of_latlong = directionPoint.size();
				for (; ii < size_of_latlong; ii++) {
					if (ii == 0) {
						PolylineOptions rectLine = new PolylineOptions().width(
								8).color(Color.RED);
						rectLine.add(my_latlong, directionPoint.get(ii));
						Polyline polyline = map.addPolyline(rectLine);
						polylines.add(polyline);
					} else {
						PolylineOptions rectLine = new PolylineOptions().width(
								8).color(Color.RED);
						rectLine.add(directionPoint.get(ii - 1),
								directionPoint.get(ii));
						Polyline polyline = map.addPolyline(rectLine);
						polylines.add(polyline);
					}
				}
				PolylineOptions rectLine = new PolylineOptions().width(8)
						.color(Color.RED);
				rectLine.add(frnd_latlong, directionPoint.get(ii - 1));
				Polyline polyline = map.addPolyline(rectLine);
				polylines.add(polyline);
				// map.addPolyline(rectLine);
			}
		}

	}*/

    public ArrayList<LatLng> getDirection(Document doc) {
        NodeList nl1, nl2, nl3;
        ArrayList<LatLng> listGeopoints = new ArrayList<LatLng>();
        nl1 = doc.getElementsByTagName("step");
        if (nl1.getLength() > 0) {
            for (int i = 0; i < nl1.getLength(); i++) {
                Node node1 = nl1.item(i);
                nl2 = node1.getChildNodes();

                Node locationNode = nl2
                        .item(getNodeIndex(nl2, "start_location"));
                nl3 = locationNode.getChildNodes();
                Node latNode = nl3.item(getNodeIndex(nl3, "lat"));
                double lat = Double.parseDouble(latNode.getTextContent());
                Node lngNode = nl3.item(getNodeIndex(nl3, "lng"));
                double lng = Double.parseDouble(lngNode.getTextContent());
                listGeopoints.add(new LatLng(lat, lng));

                locationNode = nl2.item(getNodeIndex(nl2, "polyline"));
                nl3 = locationNode.getChildNodes();
                latNode = nl3.item(getNodeIndex(nl3, "points"));
                ArrayList<LatLng> arr = decodePoly(latNode.getTextContent());
                for (int j = 0; j < arr.size(); j++) {
                    listGeopoints.add(new LatLng(arr.get(j).latitude, arr
                            .get(j).longitude));
                }

                locationNode = nl2.item(getNodeIndex(nl2, "end_location"));
                nl3 = locationNode.getChildNodes();
                latNode = nl3.item(getNodeIndex(nl3, "lat"));
                lat = Double.parseDouble(latNode.getTextContent());
                lngNode = nl3.item(getNodeIndex(nl3, "lng"));
                lng = Double.parseDouble(lngNode.getTextContent());
                listGeopoints.add(new LatLng(lat, lng));
            }
        }

        return listGeopoints;
    }

    private int getNodeIndex(NodeList nl, String nodename) {
        for (int i = 0; i < nl.getLength(); i++) {
            if (nl.item(i).getNodeName().equals(nodename))
                return i;
        }
        return -1;
    }

    private ArrayList<LatLng> decodePoly(String encoded) {
        ArrayList<LatLng> poly = new ArrayList<LatLng>();
        int index = 0, len = encoded.length();
        int lat = 0, lng = 0;
        while (index < len) {
            int b, shift = 0, result = 0;
            do {
                b = encoded.charAt(index++) - 63;
                result |= (b & 0x1f) << shift;
                shift += 5;
            } while (b >= 0x20);
            int dlat = ((result & 1) != 0 ? ~(result >> 1) : (result >> 1));
            lat += dlat;
            shift = 0;
            result = 0;
            do {
                b = encoded.charAt(index++) - 63;
                result |= (b & 0x1f) << shift;
                shift += 5;
            } while (b >= 0x20);
            int dlng = ((result & 1) != 0 ? ~(result >> 1) : (result >> 1));
            lng += dlng;

            LatLng position = new LatLng((double) lat / 1E5, (double) lng / 1E5);
            poly.add(position);
        }
        return poly;
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        finish();
        // employees.close();
        // shops.close();
        // db.close();
    }
    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putString("DO NOT CRASH", "OK");
    }

    // add items into spinner dynamically

    public void addItemsToSpinner() {

  //      ArrayList<Category> list = db.getCategories();

        // Custom ArrayAdapter with spinner item layout to set popup background

  //      CustomSpinnerAdapter spinAdapter = new CustomSpinnerAdapter(
  //              getApplicationContext(), list);

        // Default ArrayAdapter with default spinner item layout, getting some
        // view rendering problem in lollypop device, need to test in other
        // devices

		/*
		 * ArrayAdapter<String> spinAdapter = new ArrayAdapter<String>(this,
		 * android.R.layout.simple_spinner_item, list);
		 * spinAdapter.setDropDownViewResource
		 * (android.R.layout.simple_spinner_dropdown_item);
		 */

    //    spinner_nav.setAdapter(spinAdapter);

        spinner_nav.setOnItemSelectedListener(new OnItemSelectedListener() {

            @Override
            public void onItemSelected(AdapterView<?> adapter, View v,
                                       int position, long id) {
                // On selecting a spinner item
                Category item = (Category) adapter.getItemAtPosition(position);

                // Showing selected spinner item
//				Toast.makeText(getApplicationContext(),
//						"Selected  : " + item.getName(), Toast.LENGTH_LONG)
//						.show();
                selectedCategory=item.getId();
                selectedCategoryName=item.getName();
                refreshActivity();
            }

            @Override
            public void onNothingSelected(AdapterView<?> arg0) {
                // TODO Auto-generated method stub

            }
        });

    }

    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        //mSearchAction = menu.findItem(R.id.action_search);
       // boolean drawerOpen = mDrawerLayout.isDrawerOpen(mDrawerList);
       // menu.findItem(R.id.action_settings).setVisible(!drawerOpen);
        return super.onPrepareOptionsMenu(menu);
    }

    /**
     * Diplaying fragment view for selected nav drawer list item
     * */
    private void displayView(int position) {
        Fragment fragment = null;
        fragment = null;//new ShopAdd();
        // update the main content by replacing fragments
        //Fragment fragment = null;
        Log.e("side bar position", position + "");
        switch (position) {
          /*  case 0:
                fragment = new HomeFragment();
                break;
            case 1:
                fragment = new FindPeopleFragment();
                break;
            case 2:
                fragment = new PhotosFragment();
                break; */
            case 3:
                fragment =new NoteFragment();
                break;


            default:
                break;
        }

        if (fragment != null) {
            FragmentManager fragmentManager = getFragmentManager();
            fragmentManager.beginTransaction().replace(R.id.frame_container, fragment).commit();


        } else {
            // error in creating fragment
            Log.e("MainActivity", "Error in creating fragment");
        }
    }




    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (drawerToggle.onOptionsItemSelected(item))
            return true;

        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();



        return super.onOptionsItemSelected(item);
    }
    @Override
    public void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        drawerToggle.syncState();
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        drawerToggle.onConfigurationChanged(newConfig);
    }

    protected void handleMenuSearch() {
        ActionBar action = getSupportActionBar(); // get the actionbar

        if (isSearchOpened) { // test if the search is open

            action.setDisplayShowCustomEnabled(false); // disable a custom view
            // inside the actionbar
            action.setDisplayShowTitleEnabled(true); // show the title in the
            // action bar

            // hides the keyboard
            InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(edtSeach.getWindowToken(), 0);

            // add the search icon in the action bar
            mSearchAction.setIcon(getResources().getDrawable(R.drawable.ic_action_search));
            // edtSeach.requestFocus();
            isSearchOpened = false;
        } else { // open the search entry

            action.setDisplayShowCustomEnabled(true); // enable it to display a
            // custom view in the action bar.
            action.setCustomView(R.layout.search_bar);// add the custom view
            action.setDisplayShowTitleEnabled(false); // hide the title

            edtSeach = (EditText) action.getCustomView().findViewById(
                    R.id.edtSearch); // the text editor

            // this is a listener to do a search when the user clicks on search
            // button
            edtSeach.setOnEditorActionListener(new TextView.OnEditorActionListener() {
                @Override
                public boolean onEditorAction(TextView v, int actionId,
                                              KeyEvent event) {
                    if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                        doSearch();
                        return true;
                    }
                    return false;
                }

            });

            edtSeach.requestFocus();

            // open the keyboard focused in the edtSearch
            InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.showSoftInput(edtSeach, InputMethodManager.SHOW_IMPLICIT);

            // add the close icon
            mSearchAction.setIcon(getResources().getDrawable(R.drawable.ic_action_cancel));

            isSearchOpened = true;
        }
    }



    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawerLayout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            int count = getFragmentManager().getBackStackEntryCount();

            if (count == 0) {
                new AlertDialog.Builder(this)
                        .setMessage("Do you really want to exit ?")
                        .setCancelable(false)
                        .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                MainActivity.this.finish();
                            }
                        })
                        .setNegativeButton("No", null)
                        .show();
                //additional code
            } else {
                getFragmentManager().popBackStack();
            }



        }
    }

    private void doSearch() {
        //
    }


    public void showSearchAb() {
        mAbSearch.setVisibility(View.GONE);
        autocompleteViewNew.setVisibility(View.VISIBLE);
        mAbCancelSearch.setVisibility(View.VISIBLE);
        mAbClearSearch.setVisibility(View.VISIBLE);
        logo.setVisibility(View.GONE);
        autocompleteViewNew.setWidth(200);
        spinner_nav.setVisibility(View.GONE);
    }
    public void showCommonAb() {
        mAbSearch.setVisibility(View.VISIBLE);
        autocompleteViewNew.setVisibility(View.GONE);
        mAbCancelSearch.setVisibility(View.GONE);
        mAbClearSearch.setVisibility(View.GONE);
        logo.setVisibility(View.VISIBLE);
        spinner_nav.setVisibility(View.VISIBLE);
    }

    private class ChangePin extends AsyncTask<String,String,String> {
        LatLng pin;
        ArrayList<Shop> shlist=null;
        @Override
        protected String doInBackground(String... description) {
            if(!description[0].equalsIgnoreCase("") && description[0].contains(",")) {
                PlaceAPI mPlaceAPI = new PlaceAPI();
                pin = mPlaceAPI.getLatLon(description[0]);
                Log.e("Place", description[0]);
            }else{
                searchFlag=true;
                PlaceAPI mPlaceAPI = new PlaceAPI();
                shlist = mPlaceAPI.getShop(description[0],description[1],MainActivity.lastLocation);

            }
            return description[0];
        }
        @Override
        protected void onPostExecute(String result) {
			
	         /*  MarkerOptions pinMo = new MarkerOptions()
				.position(new LatLng(pin.latitude, pin.longitude)).title("Pin");
	           pinMo.icon(BitmapDescriptorFactory
						.fromResource(R.drawable.long_pin_icon));
	           //pinMo.draggable(true);
	           location.setLatitude(pin.latitude);
	           location.setLongitude(pin.longitude);
	           refreshActivity();
	           if(pinMarker!=null){
		           	pinMarker.remove();
		           }
		           pinMarker=map.addMarker(pinMo);
		           pinMarker.setDraggable(true); */
            if(shlist==null) {
                map.animateCamera(CameraUpdateFactory.newLatLngZoom(pin,DEFAULT_ZOOM_LEVEL));
            }else{
                shopList.clear();
                shopList=shlist;
                map.clear();
                setMarkersOnMap();
                mAdapter.swapItems(shlist, markers);
                map.animateCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(shopList.get(0).getLat(),shopList.get(0).getLon()), DEFAULT_ZOOM_LEVEL));

            }
        }

    }


    /**
     * Background Async Task to Load all product by making HTTP Request
     * */
    class LoadShops extends AsyncTask<LatLng, LatLng, LatLng> {
        private ArrayList<Shop> sList;
        private ArrayList<Category> cList;
        private ArrayList<String> fList;
        /**
         * Before starting background thread Show Progress Dialog
         * */
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            pDialog = new ProgressDialog(MainActivity.this);
            pDialog.setMessage("Loading Data. Please wait...");
            pDialog.setIndeterminate(false);
            pDialog.setCancelable(false);
            pDialog.show();
            processFlag=false;
        }

        /**
         * getting All products from url
         * */
        protected LatLng doInBackground(LatLng... args) {
            // Building Parameters
            GetDataFromServer dao = new GetDataFromServer();
            sList=dao.getShopList(args[0]);
            cList=dao.getCatgList();
            fList=dao.getFavShopList(0,getApplicationContext());
            return null;
        }

        /**
         * After completing background task Dismiss the progress dialog
         * **/
        protected void onPostExecute(LatLng latLng) {
            // dismiss the dialog after getting all products
            pDialog.dismiss();
            try{
                runOnUiThread(new Runnable() {
                    public void run() {
                        shopList = sList;
                        ArrayList<Category> list = cList;
                        favShoplist=fList;
                        // Custom ArrayAdapter with spinner item layout to set popup background

                        CustomSpinnerAdapter spinAdapter = new CustomSpinnerAdapter(
                                getApplicationContext(), list);
                        spinner_nav.setAdapter(spinAdapter);
                        setUpMap();
                        TextView textView = (TextView) findViewById(R.id.textView);
                        textView.setText(sList.size() + " Shops");
                        setUpExpandableList();
                        if(sList.size()>0){
                            setMarkersOnMap();
                            setMarkersListener();
                        }else{
                            Toast.makeText(getApplicationContext(),
                                    "No Data Found", Toast.LENGTH_SHORT).show();
                            LinearLayout listLayout = (LinearLayout) findViewById(R.id.listLayout);
                            listLayout.setVisibility(View.GONE);
                            countLayout.setVisibility(View.VISIBLE);
                            hideButton.setVisibility(View.GONE);
                            showButton.setVisibility(View.VISIBLE);
                        }

                    }
                });
            }catch(Exception e){
                Toast.makeText(getApplicationContext(),
                        "Some Error Occured", Toast.LENGTH_SHORT).show();
            }
            processFlag=true;

        }


    }


    /**
     * Background Async Task to Load all product by making HTTP Request
     * */
    class RefreshShops extends AsyncTask<LatLng, LatLng, LatLng> {
        private ArrayList<Shop> sList;
        private ArrayList<String> fList;
        /**
         * Before starting background thread Show Progress Dialog
         * */
        @Override
        protected void onPreExecute() {
//            super.onPreExecute();
//            pDialog = new ProgressDialog(MainActivity.this);
//            pDialog.setMessage("Loading Data. Please wait...");
//            pDialog.setIndeterminate(false);
//            pDialog.setCancelable(false);
//            pDialog.show();
            processFlag=false;
            pd.setMessage("Loading Data. Please wait...");
            pd.show();
        }

        /**
         * getting All products from url
         * */
        protected LatLng doInBackground(LatLng... args) {
            // Building Parameters
            GetDataFromServer dao = new GetDataFromServer();
            //sList=dao.getShopList(args[0]);
            sList=dao.getShopListCategory(args[0],selectedCategoryName);
           // fList=dao.getFavShopList(0, getApplicationContext());

            return null;
        }

        /**
         * After completing background task Dismiss the progress dialog
         * **/
        protected void onPostExecute(LatLng latLng) {
            // dismiss the dialog after getting all products
            //pDialog.dismiss();
            runOnUiThread(new Runnable() {
                public void run() {

                    ArrayList<Shop> newshopList = sList;
                    if(mAdapter==null){
                        setUpExpandableList();
                    }
                    if(shopList!=null){
                    shopList.clear();}

                    TextView textView = (TextView) findViewById(R.id.textView);
                    textView.setText("0 Shops");
                    if(sList.size()>0 ){
                        textView.setText(sList.size() + " Shops");
                        shopList=newshopList;
                        map.clear();
                        setMarkersOnMap();

                        mAdapter.swapItems(newshopList,markers);
                    }else{
                        LinearLayout listLayout = (LinearLayout) findViewById(R.id.listLayout);
                        listLayout.setVisibility(View.GONE);
                        countLayout.setVisibility(View.VISIBLE);
                        hideButton.setVisibility(View.GONE);
                        showButton.setVisibility(View.VISIBLE);
                        Toast.makeText(getApplicationContext(),
                                "No Data Found", Toast.LENGTH_SHORT).show();
                    }

                }
            });
            pd.dismiss();
            processFlag=true;
        }




    }
    public static int getPixelsFromDp(Context context, float dp) {
        final float scale = context.getResources().getDisplayMetrics().density;
        return (int)(dp * scale + 0.5f);
    }

    @Override
    public void onMarkerDrag(Marker arg0) {
        // TODO Auto-generated method stub

    }

    @Override
    public void onMarkerDragEnd(Marker arg0) {
        // TODO Auto-generated method stub
       /* LatLng dragPosition = arg0.getPosition();
        double dragLat = dragPosition.latitude;
        double dragLong = dragPosition.longitude;
        
        MarkerOptions pinMo = new MarkerOptions()
		.position(dragPosition).title("Pin Location");
        pinMo.icon(BitmapDescriptorFactory
				.fromResource(R.drawable.long_pin_icon));
        if(pinMarker!=null){
        	pinMarker.remove();
        }
	pinMarker=map.addMarker(pinMo);
	pinMarker.setDraggable(true);
	location.setLatitude(dragPosition.latitude);
	location.setLongitude(dragPosition.longitude);
	map.animateCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(location.getLatitude(),
			location.getLongitude()),15));
	refreshActivity();
        Toast.makeText(getApplicationContext(), "Marker Dragged..!", Toast.LENGTH_LONG).show(); */
    }

    @Override
    public void onMarkerDragStart(Marker arg0) {
        // TODO Auto-generated method stub

    }

    private class CheckInternet extends AsyncTask<Void,Boolean,Boolean> {
        boolean isInternetAval=false;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            pDialog = new ProgressDialog(MainActivity.this);
            pDialog.setMessage("Loading Data. Please wait...");
            pDialog.setIndeterminate(false);
            pDialog.setCancelable(false);
            pDialog.show();
        }
        @Override
        protected Boolean doInBackground(Void... params) {

            try {
                InetAddress ipAddr = InetAddress.getByName("google.com"); //You can replace it with your name

                if (ipAddr.equals("")) {
                    isInternetAval= false;
                } else {
                    isInternetAval= true;
                }

            } catch (Exception e) {
                return false;
            }

            return isInternetAval;
        }
        @Override
        protected void onPostExecute(Boolean result) {
            pDialog.dismiss();
            isInternetAvialable=isInternetAval;
            if(isInternetAvialable){
                LatLng lc=new LatLng(location.getLatitude(),location.getLongitude());

                if(isRefresh){
                    isRefresh=false;
                    if(processFlag ){
                        new RefreshShops().execute(lc);
                    }
                }else{
                    processFlag=false;
                    new LoadShops().execute(lc);
                }
            }else{

                dialog = b.create();
                dialog.show();
            }
            isRefresh=false;
        }


    }

    private class CheckInternetWithoutDialog extends AsyncTask<String,String,String> {
        String desc="";
        boolean isInternetAval= false;
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }
        @Override
        protected String doInBackground(String... description) {
            desc=description[0];
            try {
                InetAddress ipAddr = InetAddress.getByName("google.com"); //You can replace it with your name

                if (ipAddr.equals("")) {
                    isInternetAval= false;
                } else {
                    isInternetAval= true;
                }

            } catch (Exception e) {
                return "";
            }

            return "";
        }
        @Override
        protected void onPostExecute(String result) {

            isInternetAvialable=isInternetAval;
            if(isInternetAvialable){
                lastLocation=location;
                new ChangePin().execute(desc,lastSearchedString);
            }
        }


    }

    private class CheckRefreshInternet extends AsyncTask<Void,Boolean,Boolean> {
        boolean isInternetAval=false;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

        }
        @Override
        protected Boolean doInBackground(Void... params) {

            try {
                InetAddress ipAddr = InetAddress.getByName("google.com"); //You can replace it with your name

                if (ipAddr.equals("")) {
                    isInternetAval= false;
                } else {
                    isInternetAval= true;
                }

            } catch (Exception e) {
                return false;
            }

            return isInternetAval;
        }
        @Override
        protected void onPostExecute(Boolean result) {

            isInternetAvialable=isInternetAval;
            if(isInternetAvialable){
                LatLng lc=new LatLng(location.getLatitude(),location.getLongitude());

                if(isRefresh){
                    isRefresh=false;
                    if(processFlag ){
                        new RefreshShops().execute(lc);
                    }
                }
            }
            isRefresh=false;
        }


    }

    public double CalculationByDistance(LatLng StartP, LatLng EndP) {
        int Radius = 6371;// radius of earth in Km
        double lat1 = StartP.latitude;
        double lat2 = EndP.latitude;
        double lon1 = StartP.longitude;
        double lon2 = EndP.longitude;
        double dLat = Math.toRadians(lat2 - lat1);
        double dLon = Math.toRadians(lon2 - lon1);
        double a = Math.sin(dLat / 2) * Math.sin(dLat / 2)
                + Math.cos(Math.toRadians(lat1))
                * Math.cos(Math.toRadians(lat2)) * Math.sin(dLon / 2)
                * Math.sin(dLon / 2);
        double c = 2 * Math.asin(Math.sqrt(a));
//        double valueResult = Radius * c;
//        double km = valueResult / 1;
//        DecimalFormat newFormat = new DecimalFormat("####");
//        int kmInDec = Integer.valueOf(newFormat.format(km));
//        double meter = valueResult % 1000;
//        int meterInDec = Integer.valueOf(newFormat.format(meter));
//        Log.i("Radius Value", "" + valueResult + "   KM  " + kmInDec
//                + " Meter   " + meterInDec);

        return Radius * c;
    }
    @Override
    public void selectedIndices(List<Integer> indices) {

    }
    @Override
    public void selectedStrings(List<String> strings) {
        // Toast.makeText(this, strings.toString(), Toast.LENGTH_LONG).show();
    }
    public void showTimePickerDialog(View v) {
        DialogFragment newFragment = new TimePickerFragment();
        newFragment.show(getFragmentManager(), "timePicker");
    }
    public void showTimePickerDialogend(View v) {
        DialogFragment newFragment = new TimePickerFragmentEnd();
        newFragment.show(getFragmentManager(), "timePicker");
    }
    /** calling web service to get creditors list */
    /**
     * The Class BackgroundTask.
     */
    private class BackgroundTask extends AsyncTask<String, Void, String> {

        /** The resp. */
        String resp;

        /*
         * (non-Javadoc)
         *
         * @see android.os.AsyncTask#onPreExecute()
         */
        protected void onPreExecute() {

            pd.setMessage("Submitting...");
            pd.setCancelable(false);
            pd.show();

        }

        /*
         * (non-Javadoc)
         *
         * @see android.os.AsyncTask#doInBackground(Params[])
         */
        protected String doInBackground(final String... params) {

            String shopName = params[0];
            String shopAddress = params[1];
            String shopStartTime = params[2];
            String shopEndTime = params[3];
            String lat = params[4];
            String lon = params[5];
            String primaryContact = params[6];
            String secondaryContact = params[7];
            String shopDelCharge = params[8];
            String shopMaxTime = params[9];
            String shopMinOrder = params[10];
            String selectedCategories = params[11];
            JSONParser jsonParser;
            jsonParser = new JSONParser();
            String success;
            String url="http://www.gpl4you.com/mvb/addshop.php?";
            String newUrl = url + "shopName="
                    + shopName + "&shopAddress="
                    + shopAddress;

            BasicNameValuePair shopNamev = new BasicNameValuePair("shopName", shopName);
            BasicNameValuePair shopAddressv = new BasicNameValuePair("shopAddress", shopAddress);
            BasicNameValuePair shopStartTimev = new BasicNameValuePair("shopStartTime", shopStartTime);
            BasicNameValuePair shopEndTimev = new BasicNameValuePair("shopEndTime", shopEndTime);
            BasicNameValuePair shopPrimaryContactv = new BasicNameValuePair("shopPrimaryContact", primaryContact);
            BasicNameValuePair shopSecondaryContactv = new BasicNameValuePair("shopSecondaryContact", secondaryContact);
            BasicNameValuePair shopDelChargev = new BasicNameValuePair("shopDelCharge", shopDelCharge);
            BasicNameValuePair shopMaxTimev = new BasicNameValuePair("shopMaxTime", shopMaxTime);
            BasicNameValuePair shopMinOrderv = new BasicNameValuePair("shopMinOrder", shopMinOrder);
            BasicNameValuePair latitudev = new BasicNameValuePair("latitude", lat);
            BasicNameValuePair longitudev = new BasicNameValuePair("longitude", lon);
            BasicNameValuePair selectedCategoriesv = new BasicNameValuePair("selectedCategories", selectedCategories);

            // We add the content that we want to pass with the POST request to as name-value pairs
            //Now we put those sending details to an ArrayList with type safe of NameValuePair
            List<NameValuePair> nameValuePairList = new ArrayList<NameValuePair>();
            nameValuePairList.add(shopNamev);
            nameValuePairList.add(shopAddressv);
            nameValuePairList.add(shopStartTimev);
            nameValuePairList.add(shopEndTimev);
            nameValuePairList.add(shopPrimaryContactv);
            nameValuePairList.add(shopSecondaryContactv);

            nameValuePairList.add(shopDelChargev);
            nameValuePairList.add(shopMaxTimev);
            nameValuePairList.add(shopMinOrderv);
            nameValuePairList.add(latitudev);
            nameValuePairList.add(longitudev);
            nameValuePairList.add(selectedCategoriesv);

            JSONObject json = jsonParser.makeHttpRequest(newUrl, "GET",nameValuePairList);

            try {
                /*success = json.getString("msg");
                Log.v("SUCCESS:: ", success);
                if (success.trim().toString().equalsIgnoreCase("SUCCESS")) {
                    String loggedUsername = json.getString("USER_NAME");
                    //bundle.putString("loggedUser", loggedUsername);
                    Log.e("Logged User::", loggedUsername);

                }*/
            } catch (Exception e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }

            return null;

        }

        /*
         * (non-Javadoc)
         *
         * @see android.os.AsyncTask#onPostExecute(java.lang.Object)
         */
        protected void onPostExecute(String list) {
           pd.dismiss();
            Toast.makeText(getApplicationContext(), "Shop Successfully Added", Toast.LENGTH_SHORT).show();

        }
    }



    private void selectImage() {
        final CharSequence[] items = { "Take Photo", "Choose from Library",
                "Cancel" };

        AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
        builder.setTitle("Add Photo!");
        builder.setItems(items, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int item) {
                if (items[item].equals("Take Photo")) {
                    Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                    startActivityForResult(intent, REQUEST_CAMERA);
                } else if (items[item].equals("Choose from Library")) {
                    Intent intent = new Intent(
                            Intent.ACTION_PICK,
                            android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                    intent.setType("image/*");
                    startActivityForResult(
                            Intent.createChooser(intent, "Select File"),
                            SELECT_FILE);
                } else if (items[item].equals("Cancel")) {
                    dialog.dismiss();
                }
            }
        });
        builder.show();
    }



    private void onCaptureImageResult(Intent data) {
        Bitmap thumbnail = (Bitmap) data.getExtras().get("data");
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        thumbnail.compress(Bitmap.CompressFormat.JPEG, 90, bytes);

        File destination = new File(Environment.getExternalStorageDirectory(),
                System.currentTimeMillis() + ".jpg");

        FileOutputStream fo;
        try {
            destination.createNewFile();
            fo = new FileOutputStream(destination);
            fo.write(bytes.toByteArray());
            fo.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        ivImage.setImageBitmap(thumbnail);
    }

    @SuppressWarnings("deprecation")
    private void onSelectFromGalleryResult(Intent data) {
        Uri selectedImageUri = data.getData();
        String[] projection = { MediaStore.MediaColumns.DATA };
        Cursor cursor = managedQuery(selectedImageUri, projection, null, null,
                null);
        int column_index = cursor.getColumnIndexOrThrow(MediaStore.MediaColumns.DATA);
        cursor.moveToFirst();

        String selectedImagePath = cursor.getString(column_index);

        Bitmap bm;
        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;
        BitmapFactory.decodeFile(selectedImagePath, options);
        final int REQUIRED_SIZE = 200;
        int scale = 1;
        while (options.outWidth / scale / 2 >= REQUIRED_SIZE
                && options.outHeight / scale / 2 >= REQUIRED_SIZE)
            scale *= 2;
        options.inSampleSize = scale;
        options.inJustDecodeBounds = false;
        bm = BitmapFactory.decodeFile(selectedImagePath, options);

        ivImage.setImageBitmap(bm);
    }

    public void setstarttime(String st){
        TextView txtStartDate = (TextView)myDialog.findViewById(R.id.addShopStartTimeLabel);
        txtStartDate.setText(st);
    }
    public void setendtime(String st){
        TextView txtStartDate = (TextView)myDialog.findViewById(R.id.addShopEndTimeLabel);
        txtStartDate.setText(st);
    }
    public void setMapToshop(LatLng g,int position){

        map.animateCamera(CameraUpdateFactory.newLatLngZoom(g, SHOP_ZOOM_LEVEL));
        markers.get(position).showInfoWindow();

    }
    @Override
    public void setTitle(CharSequence title) {
        mTitle = title;
        getActionBar().setTitle(mTitle);
    }


}