package com.mvb.client;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.mvb.client.models.Note;
import com.mvb.client.models.Product;
import com.mvb.client.models.ShopReview;

import java.util.ArrayList;
import java.util.List;

import com.mvb.client.dao.GetDataFromServer;

/**
 * Created by DEVLOP on 25-Mar-16.
 */
public class NotesAdapter extends BaseAdapter {

    private ImageView imgIcon;
    private TextView txtTitle;
    //private ArrayList<SpinnerNavItem> spinnerNavItem;
    private Context context;
    List<ShopReview> spinnerNavItem= new ArrayList<ShopReview>();

    public NotesAdapter(Context context,int shopId) {
        this.context = context;
        GetDataFromServer dao = new GetDataFromServer();
        List<Note> products=dao.getMyNotesList( shopId, 0, context);
        if (products!=null) {
            for (int i = 0; i < products.size(); i++) {
                ShopReview sr1 = new ShopReview();
                sr1.userName = products.get(i).getTitle();
                sr1.created = "by " + products.get(i).getName() + " on " + products.get(i).getCreated();
                sr1.review = products.get(i).getContent();
                spinnerNavItem.add(sr1);
            }
        }
       /* ShopReview sr1=new ShopReview();
        sr1.userName="Satisfied !!";
        sr1.created="by Prashant on 12 Dec'15";
        sr1.review="Not Satisfied with delivery !!";
        spinnerNavItem.add(sr1);

        ShopReview sr2=new ShopReview();
        sr2.userName="Not Satisfied";
        sr2.created="by Sahil on 12 Dec'15";
        sr2.review="Satisfied with delivery !!";
        spinnerNavItem.add(sr2);*/
    }

    @Override
    public int getCount() {
        return spinnerNavItem.size();
    }

    @Override
    public Object getItem(int index) {
        return spinnerNavItem.get(index);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (convertView == null) {
            LayoutInflater mInflater = (LayoutInflater)
                    context.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
            convertView = mInflater.inflate(R.layout.notes, null);
        }

        TextView userName = (TextView) convertView.findViewById(R.id.username);
        userName.setText(spinnerNavItem.get(position).getUserName());

        TextView noteContent = (TextView) convertView.findViewById(R.id.notecontent);
        noteContent.setText(spinnerNavItem.get(position).getReview());

        TextView noteTime = (TextView) convertView.findViewById(R.id.notetime);
        noteTime.setText(spinnerNavItem.get(position).getCreated());
        return convertView;
    }



}
