package com.mvb.client;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Rect;
import android.net.Uri;
import android.os.AsyncTask;
import android.provider.Settings;
import android.provider.Settings.Secure;
import android.util.Log;
import android.view.Display;
import android.view.GestureDetector;
import android.view.GestureDetector.SimpleOnGestureListener;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.HorizontalScrollView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.LinearLayout.LayoutParams;
import android.widget.ListView;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.TabHost;
import android.widget.TabHost.OnTabChangeListener;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.mvb.client.models.Category;
import com.mvb.client.models.Product;
import com.mvb.client.models.ShopReview;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class ShopListAdapter extends BaseAdapter{

    public ArrayList<Shop> mainList;
    public ArrayList<Marker> markers;
    public ArrayList<String> favShopList;
    private ArrayList<Boolean> checkList = new ArrayList<Boolean>();
    public GoogleMap map;
    public Context appContext;
    static TabHost mytabs;
    LinearLayout asthmaActionPlan, controlledMedication, asNeededMedication,
            rescueMedication, yourSymtoms, yourTriggers, wheezeRate, peakFlow;
    LayoutParams params;
    LinearLayout next, prev;
    int viewWidth;
    GestureDetector gestureDetector = null;
    HorizontalScrollView horizontalScrollView,horizontalScrollViewtab2;
    ArrayList<LinearLayout> layouts,layoutstab2;
    int parentLeft, parentRight;
    int mWidth;
    int currPosition, prevPosition;
    AlertDialog.Builder b;
    ProgressDialog pd ;
    public int mSelected = -1;
    Dialog myDialog=null;
    public ShopListAdapter(Context applicationContext,
                           ArrayList<Shop> questionForSliderMenu,ArrayList<Marker> markers,GoogleMap map,ArrayList<String>favShopList) {

        super();

        this.mainList = questionForSliderMenu;
        this.markers = markers;
        this.appContext=applicationContext;
        this.map=map;
        this.favShopList=favShopList;
        for (int i = 0; i < mainList.size(); i++) {
            checkList.add(i,false);
        }
        if(this.favShopList!=null){
            for (int ii = 0; ii < mainList.size(); ii++) {
                for (int jj = 0; jj < favShopList.size(); jj++) {
                    if (favShopList.get(jj).equalsIgnoreCase(mainList.get(ii).getId() + "")) {
                        checkList.set(ii,true);
                    }
                }

            }
        }

        this.pd=new ProgressDialog(applicationContext) ;
    }


    public ShopListAdapter() {

        super();

    }

    @Override
    public int getCount() {

        return mainList.size();
    }

    @Override
    public Shop getItem(int position) {

        return mainList.get(position);
    }

    public Marker getMarker(int position) {

        return markers.get(position);
    }

    public GoogleMap getMap(int position) {

        return map;
    }

    @Override
    public long getItemId(int position) {

        return position;
    }


    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        final View cView=convertView;

        if (convertView == null) {
            WindowManager wm = (WindowManager)this.appContext.getSystemService(Context.WINDOW_SERVICE);
            Display display = wm.getDefaultDisplay();
            mWidth = display.getWidth(); // deprecated
            viewWidth = mWidth / 3;
            layouts = new ArrayList<LinearLayout>();
            params = new LayoutParams(viewWidth, LayoutParams.WRAP_CONTENT);
            LayoutInflater inflater = (LayoutInflater) this.appContext
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.shoplist, null);


//            parent.getChildAt(position).setBackgroundColor(appContext.getResources().getColor(R.color.list_dark_green));
//            
            convertView.setBackgroundColor(appContext.getResources().getColor(R.color.list_light_green));
            if ( mSelected == position){
                convertView.setBackgroundColor(appContext.getResources().getColor(R.color.list_dark_green));
            }
//
//            mSelected = position; 

     /*       final TabHost mTabHost = (TabHost) convertView.findViewById(R.id.tabhost);
            mTabHost.setup();



            mTabHost.addTab(mTabHost.newTabSpec("tab_test1").setIndicator(getProductTabIndicator()).setContent(R.id.hsv));

            //mTabHost.addTab(mTabHost.newTabSpec("tab_test1").setIndicator("Products").setContent(R.id.hsv));
            mTabHost.addTab(mTabHost.newTabSpec("tab_test2").setIndicator(getReviewTabIndicator()).setContent(R.id.review_tab));
            mTabHost.addTab(mTabHost.newTabSpec("tab_test3").setIndicator(getRateTabIndicator()).setContent(R.id.review_submit_tab));
            mTabHost.setCurrentTab(0);


            //prev = (LinearLayout) convertView.findViewById(R.id.prev);
            //next = (LinearLayout) convertView.findViewById(R.id.next);
            horizontalScrollView = (HorizontalScrollView) convertView.findViewById(R.id.hsv);


            LayoutInflater viproducts = (LayoutInflater) this.appContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

            MyDatabase db;
            db = new MyDatabase(this.appContext);
            List<Product> products=db.getProductsByShopId(getItem(position).getId());
            List<ShopReview> shopreviewList=db.getReviewsByShopId(getItem(position).getId());
            db.close();
            for(int i=0;i<products.size();i++){
                View vproducts = viproducts.inflate(R.layout.products_row, null);

                TextView productName = (TextView) vproducts.findViewById(R.id.productName);
                productName.setText(products.get(i).getName());

                TextView productDetail = (TextView) vproducts.findViewById(R.id.productDetail);
                productDetail.setText(products.get(i).getDetails());

                TextView productPrice = (TextView) vproducts.findViewById(R.id.productPrice);
                productPrice.setText("Rs." +products.get(i).getPrice());

                ViewGroup productGroup = (ViewGroup) convertView.findViewById(R.id.productinnerLay);
                productGroup.addView(vproducts, 0);
            }





            layoutstab2 = new ArrayList<LinearLayout>();

            for(int i=0;i<shopreviewList.size();i++){
                View vshopreview = viproducts.inflate(R.layout.review_row, null);

                TextView shopreviewName = (TextView) vshopreview.findViewById(R.id.reviewName);
                shopreviewName.setText(shopreviewList.get(i).getUserName());

                TextView shopreviewTime = (TextView) vshopreview.findViewById(R.id.reviewDate);
                shopreviewTime.setText(shopreviewList.get(i).getCreated());

                TextView shopreview = (TextView) vshopreview.findViewById(R.id.reviewText);
                shopreview.setText(shopreviewList.get(i).getReview());

                ViewGroup shopReviewGroup = (ViewGroup) convertView.findViewById(R.id.reviewInnerLay);
                shopReviewGroup.addView(vshopreview, 0);
            }

            horizontalScrollViewtab2 = (HorizontalScrollView) convertView.findViewById(R.id.review_tab);


            View vaddreview = viproducts.inflate(R.layout.rate_row, null);

//           	TextView vaddreviewText = (TextView) vaddreview.findViewById(R.id.reviewText);
            //shopreviewName.setText(shopreviewList.get(i).getUserName());
            //v.setLayoutParams(params);

//            layoutstab2.add((LinearLayout) v);
//            layoutstab2.add((LinearLayout)v2);
            //TextView vaddreviewText = (TextView) vaddreview.findViewById(R.id.reviewAddText);
            RadioGroup vratingbar = (RadioGroup) vaddreview.findViewById(R.id.radioRate);
            LayoutParams params1 = new LayoutParams(mWidth, LayoutParams.WRAP_CONTENT);
            //vratingbar.setLayoutParams(params);
            TextView vrevtext = (TextView) vaddreview.findViewById(R.id.rateHeader);
            Button vimgbtn = (Button) vaddreview.findViewById(R.id.reviewSubmit);
            ViewGroup shopReviewSubmit = (ViewGroup) convertView.findViewById(R.id.review_submit_tab);
            vaddreview.setLayoutParams(params1);
            shopReviewSubmit.addView(vaddreview, 0);


            mTabHost.setOnTabChangedListener(new OnTabChangeListener(){
                @Override
                public void onTabChanged(String tabId) {
                    if("tab_test1".equals(tabId)) {
                        Log.i("Inside tab1","Inside tab1");
                    }
                    if("tab_test2".equals(tabId)) {
                        Log.i("Inside tab2","Inside tab2");

                    }
                    if("tab_test3".equals(tabId)) {
                        Log.i("Inside tab3","Inside tab3");

                    }
                }});
            */

        }


        TextView txtshopName = (TextView) convertView
                .findViewById(R.id.shopName);
        TextView txtshopAddress = (TextView) convertView
                .findViewById(R.id.shopAddress);
        TextView txtcategories = (TextView) convertView
                .findViewById(R.id.categories);
        TextView txtdeliveryIn = (TextView) convertView
                .findViewById(R.id.deliveryIn);
        TextView txtdeliveryMinimum = (TextView) convertView
                .findViewById(R.id.deliveryMinimum);
//        TextView txtdeliveryTiming = (TextView) convertView
//                .findViewById(R.id.deliveryTiming);
        TextView txtdeliveryCharge = (TextView) convertView
                .findViewById(R.id.deliveryCharge);
        TextView txtmaxDistance = (TextView) convertView
                .findViewById(R.id.maxDistance);
        View toggleButton= (View) convertView
                .findViewById(R.id.expandable_toggle_button);

        final ImageView imageIcon = (ImageView) convertView
                .findViewById(R.id.shopImageIcon);
//        ImageView imageClick = (ImageView) convertView
//                .findViewById(R.id.listButtonImg);

        ImageView reviewcallBtnClick = (ImageView) convertView
                .findViewById(R.id.reviewButtonImg);
        ImageView callBtnClick = (ImageView) convertView
                .findViewById(R.id.callButtonImg);
        ImageView favBtnClick = (ImageView) convertView
                .findViewById(R.id.favImg);
        LinearLayout expandable =(LinearLayout) convertView.findViewById(R.id.list_item_select);

        try {
            final Shop shop=this.getItem(position);
             boolean isFav=false;
            txtshopName.setText(shop.getName());
            txtshopAddress.setText(shop.getAddress());
            txtcategories.setText( " ( "+ shop.getDistance() +" km ) "+shop.getMainCategory());
            txtdeliveryIn.setText(shop.getMaxTime());
            txtdeliveryMinimum.setText(shop.getMinOrder());
            //txtdeliveryTiming.setText(shop.getStartTime()+"-"+shop.getEndTime() );
            txtdeliveryCharge.setText(shop.getCharge());
            //txtmaxDistance.setText(shop.getMaxDistance()+" Km");
            txtmaxDistance.setText(shop.getDistance() + " Km");
            /*if(this.favShopList!=null){
                for (int ii = 0; ii < favShopList.size(); ii++) {
                    if(favShopList.get(ii).equalsIgnoreCase(shop.getId()+ "")){
                        //Log.e("shop_id",shop.getId() + "");
                        favBtnClick.setImageResource(R.drawable.star);
                        isFav=true;
                    }else{
                        favBtnClick.setImageResource(R.drawable.star_default);
                    }
                }
            } */
            favBtnClick.setTag(Integer.valueOf(position));
            if(checkList.get(Integer.parseInt(favBtnClick.getTag().toString()))){
                favBtnClick.setImageResource(R.drawable.star);
                imageIcon.setImageResource(R.drawable.locpin_fav);
                isFav=true;
            }else{
                favBtnClick.setImageResource(R.drawable.star_default);
                imageIcon.setImageResource(R.drawable.locpin);
            }

            final boolean isShopFav=isFav;
            /*if(shop.getMainCategory()==null || shop.getMainCategory().equalsIgnoreCase("")){
                imageIcon.setImageResource(R.drawable.atta);
            }else{
                for(String retval:shop.getMainCategory().split(",")){
                    String category=retval.toLowerCase();
                    if(retval.toLowerCase().equals("grocery")){	imageIcon.setImageResource(R.drawable.grocery);}
                    else if(retval.toLowerCase().equals("bakery")){	imageIcon.setImageResource(R.drawable.bakery);}
                    else if(retval.toLowerCase().equals("flour")){	imageIcon.setImageResource(R.drawable.atta);}
                    else if(retval.toLowerCase().equals("dairy")){	imageIcon.setImageResource(R.drawable.dairy);}
                    else if(retval.toLowerCase().equals("flowers")){	imageIcon.setImageResource(R.drawable.organic_food);}
                    else if(retval.toLowerCase().equals("fruits")){	imageIcon.setImageResource(R.drawable.fruits);}
                    else if(retval.toLowerCase().equals("sweets")){	imageIcon.setImageResource(R.drawable.sweets);}
                    else if(retval.toLowerCase().equals("vegetables")){	imageIcon.setImageResource(R.drawable.vegetables);}
                    else if(retval.toLowerCase().equals("water")){	imageIcon.setImageResource(R.drawable.water);}
                    else{
                        imageIcon.setImageResource(R.drawable.grocery);
                    }
                }
            } */
        	
        	
            /*imageClick.setOnClickListener(new OnClickListener() {

                @Override
                public void onClick(View v) {



                    switch (v.getId()) {
                    case R.id.listButtonImg:

                        PopupMenu popup = new PopupMenu(appContext, v);
                        popup.getMenuInflater().inflate(R.menu.menu,
                                popup.getMenu());
                        popup.show();
                        popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                            @Override
                            public boolean onMenuItemClick(MenuItem item) {

                                switch (item.getItemId()) {
                                case R.id.install:

                                    //Or Some other code you want to put here.. This is just an example.
                                    Toast.makeText(appContext, " Install Clicked at position " + " : " + position, Toast.LENGTH_LONG).show();

                                    break;
                                case R.id.addtowishlist:

                                    Toast.makeText(appContext, "Add to Wish List Clicked at position " + " : " + position, Toast.LENGTH_LONG).show();

                                    break;

                                default:
                                    break;
                                }

                                return true;
                            }
                        });

                        break;
                        
                 

                    default:
                        break;
                    }



                }
            }); */



            reviewcallBtnClick.setOnClickListener(new OnClickListener() {

                @Override
                public void onClick(View v) {
                    pd.setMessage("Loading...");
                    pd.setCancelable(true);
                    pd.show();
                    Log.e("click add","on note button clickkk");
                    myDialog = new Dialog(appContext);
                    myDialog.setCancelable(true);
                    myDialog.setTitle("Add Note");
                    WindowManager manager = (WindowManager) appContext.getSystemService(Context.WINDOW_SERVICE);
                    int width, height;
                    WindowManager.LayoutParams params;
                    width = manager.getDefaultDisplay().getWidth();
                    height = manager.getDefaultDisplay().getHeight();
                    WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
                    lp.copyFrom(myDialog.getWindow().getAttributes());
                    lp.width = width;
                    lp.height = height+200;
                    myDialog.getWindow().setAttributes(lp);

                    LayoutInflater viproducts = (LayoutInflater) appContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                    View noteview=viproducts.inflate(R.layout.add_note, null);

                    ListView lstcontent = (ListView) noteview.findViewById(R.id.noteslist);
                    NotesAdapter adapter;

                    adapter = new NotesAdapter(appContext,shop.getId());
                    lstcontent.setAdapter(adapter);

                    myDialog.setContentView(noteview);
                    myDialog.show();
                    pd.dismiss();
                    final EditText name=(EditText)myDialog.findViewById(R.id.addNoteName);
                    final EditText content=(EditText)myDialog.findViewById(R.id.addNoteContent);
                    final EditText title=(EditText)myDialog.findViewById(R.id.addNoteTitle);
                    final CheckBox isPublic=(CheckBox)myDialog.findViewById(R.id.checkBox);
                    //ivImage = (ImageView) myDialog.findViewById(R.id.ivImage);
                    Button addShopSubmit=(Button)myDialog.findViewById(R.id.addNoteSubmit);



                    addShopSubmit.setOnClickListener(new OnClickListener()
                    {

                        @Override
                        public void onClick(View v)
                        {

                            Log.e("name",name.getText().toString());
                            Log.e("content",content.getText().toString());
                            Log.e("title",title.getText().toString());
                            Log.e("isPublic", isPublic.getText().toString());
                            int is_public=0;
                            if(isPublic.isEnabled()){
                                is_public=1;
                            }
                            String android_id = Settings.Secure.getString(appContext.getContentResolver(),
                                    Settings.Secure.ANDROID_ID);
                            String nm=name.getText().toString();
                            String ti=title.getText().toString();
                            String cn=content.getText().toString();
                            String ip=is_public+"";
                            new AddNoteTask().execute(nm, ti, cn, ip,android_id,shop.getId()+"");
                        }
                    });
                }

            });

            callBtnClick.setOnClickListener(new OnClickListener() {

                @Override
                public void onClick(View v) {
                    String phno = "10digits";
                    Log.i("ACTIONCALL", "tel:" + shop.getMobile());
                    String mobile = shop.getMobile();
                    String[] parts = mobile.split(",");
                    Intent callIntent = new Intent(Intent.ACTION_CALL);
                    callIntent.setData(Uri.parse("tel:" + parts[0]));
                    String phone = parts[0];
                    double lat = MainActivity.lastLocation.getLatitude();
                    double lon = MainActivity.lastLocation.getLongitude();
                    String android_id = Secure.getString(appContext.getContentResolver(),
                            Secure.ANDROID_ID);
                    Integer shopId = shop.getId();
                    sendPostRequest(phone, String.valueOf(lat), String.valueOf(lon),android_id,String.valueOf(shopId), MainActivity.emailId);




                    appContext.startActivity(callIntent);
//            	    Intent i=new Intent(Intent.ACTION_CALL,Uri.parse("tel:"+shop.getMobile()), appContext,MainActivity.class);
//            	    appContext.startActivity(i);
                }


            });

            expandable.setOnClickListener(new OnClickListener() {

                @Override
                public void onClick(View v) {
                    Log.e("ItemClikc",shop.getName());
                    ((MainActivity) appContext).setMapToshop(new LatLng(shop.getLat(), shop.getLon()),position);
                }


            });
            final ImageView favbtnfinal=favBtnClick;
            favBtnClick.setOnClickListener(new OnClickListener() {

                @Override
                public void onClick(View v) {
                  //  Log.e("favbtnclick","clickkkkkkk");
                    b =new AlertDialog.Builder(appContext);
                    if(isShopFav){


                        b.setTitle("Unmark as Favorite");
                  //      Log.e("favbtnclick", "Unmark");
                    } else{
                        b.setTitle("Mark as Favorite");
                   //     Log.e("favbtnclick", "Mark");
                    }
                    //b.show();
                   // b.setMessage("Choose Option!!");


                    b.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            String android_id = Settings.Secure.getString(appContext.getContentResolver(),
                                    Settings.Secure.ANDROID_ID);
                            if (isShopFav) {
                                favbtnfinal.setImageResource(R.drawable.star_default);
                                imageIcon.setImageResource(R.drawable.locpin);
                            } else {
                                favbtnfinal.setImageResource(R.drawable.star);
                                imageIcon.setImageResource(R.drawable.locpin_fav);
                            }

                            favShopList.add(shop.getId()+"");
                            new FavShopTask().execute(android_id, shop.getId() + "");
                            dialog.dismiss();
                        }
                    });
                    b.setNegativeButton("No", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            // if this button is clicked, just close
                            // the dialog box and do nothing
                            dialog.dismiss();
                        }
                    });
                    AlertDialog alert = b.create();
                    alert.show();
                }


            });

            toggleButton.setOnClickListener(new OnClickListener() {

                @Override
                public void onClick(View v) {
                    String phno="10digits";
                    Log.i("toggleButton","tel:"+shop.getMobile());

                }

            });

        } catch (Exception e) {

            e.printStackTrace();
        }

        return convertView;
    }


    private void sendPostRequest(String phone, String lat, String lon, String androidId, String shopId, String emaiId ) {

        class SendPostReqAsyncTask extends AsyncTask<String, Void, String> {

            @Override
            protected String doInBackground(String... params) {
                String phone = params[0];
                String lat = params[1];
                String lon = params[2];
                String androidId = params[3];
                String shopId = params[4];
                String emailId = params[5];


                HttpClient httpClient = new DefaultHttpClient();

                // In a POST request, we don't pass the values in the URL.
                //Therefore we use only the web page URL as the parameter of the HttpPost argument
                HttpPost httpPost = new HttpPost("http://gpl4you.com/mvb/call_log.php");

                // Because we are not passing values over the URL, we should have a mechanism to pass the values that can be
                //uniquely separate by the other end.
                //To achieve that we use BasicNameValuePair
                //Things we need to pass with the POST request
                BasicNameValuePair phoneBasicNameValuePair = new BasicNameValuePair("phone", phone);
                BasicNameValuePair latBasicNameValuePAir = new BasicNameValuePair("lat", lat);
                BasicNameValuePair lonBasicNameValuePAir = new BasicNameValuePair("lon", lon);
                BasicNameValuePair androidIdBasicNameValuePAir = new BasicNameValuePair("android_id", androidId);
                BasicNameValuePair shopIdBasicNameValuePAir = new BasicNameValuePair("shop_id", shopId);
                BasicNameValuePair emailIdBasicNameValuePAir = new BasicNameValuePair("email_id", emailId);

                // We add the content that we want to pass with the POST request to as name-value pairs
                //Now we put those sending details to an ArrayList with type safe of NameValuePair
                List<NameValuePair> nameValuePairList = new ArrayList<NameValuePair>();
                nameValuePairList.add(phoneBasicNameValuePair);
                nameValuePairList.add(latBasicNameValuePAir);
                nameValuePairList.add(lonBasicNameValuePAir);
                nameValuePairList.add(androidIdBasicNameValuePAir);
                nameValuePairList.add(shopIdBasicNameValuePAir);
                nameValuePairList.add(emailIdBasicNameValuePAir);

                try {
                    // UrlEncodedFormEntity is an entity composed of a list of url-encoded pairs.
                    //This is typically useful while sending an HTTP POST request.
                    UrlEncodedFormEntity urlEncodedFormEntity = new UrlEncodedFormEntity(nameValuePairList);

                    // setEntity() hands the entity (here it is urlEncodedFormEntity) to the request.
                    httpPost.setEntity(urlEncodedFormEntity);

                    try {
                        // HttpResponse is an interface just like HttpPost.
                        //Therefore we can't initialize them
                        HttpResponse httpResponse = httpClient.execute(httpPost);

                        // According to the JAVA API, InputStream constructor do nothing.
                        //So we can't initialize InputStream although it is not an interface
                        InputStream inputStream = httpResponse.getEntity().getContent();

                        InputStreamReader inputStreamReader = new InputStreamReader(inputStream);

                        BufferedReader bufferedReader = new BufferedReader(inputStreamReader);

                        StringBuilder stringBuilder = new StringBuilder();

                        String bufferedStrChunk = null;

                        while((bufferedStrChunk = bufferedReader.readLine()) != null){
                            stringBuilder.append(bufferedStrChunk);
                        }

                        return stringBuilder.toString();

                    } catch (ClientProtocolException cpe) {
                        System.out.println("First Exception caz of HttpResponese :" + cpe);
                        cpe.printStackTrace();
                    } catch (IOException ioe) {
                        System.out.println("Second Exception caz of HttpResponse :" + ioe);
                        ioe.printStackTrace();
                    }

                } catch (UnsupportedEncodingException uee) {
                    System.out.println("An Exception given because of UrlEncodedFormEntity argument :" + uee);
                    uee.printStackTrace();
                }

                return null;
            }
            @Override
            protected void onPostExecute(String result) {
                super.onPostExecute(result);
                Log.e("logg",result);
                if(result==null){
                    return;
                }
                if(result.equals("done")){
                    //Toast.makeText(appContext, "HTTP POST is working...", Toast.LENGTH_LONG).show();
                }else{
                    //Toast.makeText(appContext, "Invalid POST req...", Toast.LENGTH_LONG).show();
                }
            }


        }
        SendPostReqAsyncTask sendPostReqAsyncTask = new SendPostReqAsyncTask();
        sendPostReqAsyncTask.execute(phone, lat, lon, androidId, shopId, emaiId);

    }

    class MyGestureDetector extends SimpleOnGestureListener {
        @Override
        public boolean onFling(MotionEvent e1, MotionEvent e2, float velocityX,
                               float velocityY) {
            if (e1.getX() < e2.getX()) {
                currPosition = getVisibleViews("left");
            } else {
                currPosition = getVisibleViews("right");
            }

            horizontalScrollViewtab2.smoothScrollTo(layoutstab2.get(currPosition)
                    .getLeft(), 0);
            return true;
        }
    }

    public int getVisibleViews(String direction) {
        Rect hitRect = new Rect();
        int position = 0;
        int rightCounter = 0;
        for (int i = 0; i < layoutstab2.size(); i++) {
            if (layoutstab2.get(i).getLocalVisibleRect(hitRect)) {
                if (direction.equals("left")) {
                    position = i;
                    break;
                } else if (direction.equals("right")) {
                    rightCounter++;
                    position = i;
                    if (rightCounter == 2)
                        break;
                }
            }
        }
        return position;
    }

    public void swapItems(ArrayList<Shop> items,ArrayList<Marker> markers) {
        this.mainList = items;
        this.markers = markers;
        checkList.clear();
        for (int i = 0; i < mainList.size(); i++) {
            checkList.add(i, false);
        }
        if(this.favShopList!=null){
            for (int ii = 0; ii < mainList.size(); ii++) {
                for (int jj = 0; jj < favShopList.size(); jj++) {
                    if (favShopList.get(jj).equalsIgnoreCase(mainList.get(ii).getId() + "")) {
                        checkList.set(ii,true);
                    }
                }

            }
        }
        notifyDataSetChanged();
    }

    public void addShop(Shop shop,Marker marker) {
        this.mainList.add(0,shop);
        this.markers.add(0,marker);
        notifyDataSetChanged();
    }

    public String getShopCatgIcon(int position,View view){
        Shop shop=this.getItem(position);
        boolean isGrocery=false;
        String category="";
        if(shop.getMainCategory()==null || shop.getMainCategory().equalsIgnoreCase("")){

        }else{
            String retval=shop.getMainCategory().split(",")[0];
            category=retval.toLowerCase();
            if(retval.toLowerCase().equals("grocery")){	isGrocery=true;}
            else if(retval.toLowerCase().equals("grocery")){	isGrocery=true;}
            else if(retval.toLowerCase().equals("grocery")){	isGrocery=true;}
            else if(retval.toLowerCase().equals("grocery")){	isGrocery=true;}
            else if(retval.toLowerCase().equals("grocery")){	isGrocery=true;}
            else if(retval.toLowerCase().equals("grocery")){	isGrocery=true;}
            else if(retval.toLowerCase().equals("grocery")){	isGrocery=true;}
            else if(retval.toLowerCase().equals("grocery")){	isGrocery=true;}
        }
        if(isGrocery){
            return "grocery";
        }else{
            return category;
        }

    }
    public View getProductTabIndicator(){

        View tabIndicator = LayoutInflater.from(appContext).inflate(R.layout.tab_indicator, null);
        ImageView icon = (ImageView) tabIndicator.findViewById(R.id.tabicon);
        icon.setImageResource(R.drawable.product_tab_selector);
        icon.setLayoutParams(params);
        icon.setScaleType(ImageView.ScaleType.FIT_XY);
        return tabIndicator;
    }

    public View getReviewTabIndicator(){
        View tabIndicator = LayoutInflater.from(appContext).inflate(R.layout.tab_indicator, null);
        ImageView icon = (ImageView) tabIndicator.findViewById(R.id.tabicon);
        icon.setImageResource(R.drawable.reviews_tab_selector);
        icon.setLayoutParams(params);
        icon.setScaleType(ImageView.ScaleType.FIT_XY);
        return tabIndicator;
    }
    public View getRateTabIndicator(){
        View tabIndicator = LayoutInflater.from(appContext).inflate(R.layout.tab_indicator, null);
        ImageView icon = (ImageView) tabIndicator.findViewById(R.id.tabicon);
        icon.setImageResource(R.drawable.rate_tab_selector);
        icon.setLayoutParams(params);
        icon.setScaleType(ImageView.ScaleType.FIT_XY);
        return tabIndicator;
    }



    private class AddNoteTask extends AsyncTask<String, Void, String> {

        /** The resp. */
        String resp;

        /*
         * (non-Javadoc)
         *
         * @see android.os.AsyncTask#onPreExecute()
         */
        protected void onPreExecute() {

            pd.setMessage("Submitting...");
            pd.setCancelable(true);
            pd.show();

        }

        /*
         * (non-Javadoc)
         *
         * @see android.os.AsyncTask#doInBackground(Params[])
         */
        protected String doInBackground(final String... params) {

            String name = params[0];
            String title = params[1];
            String content = params[2];
            String ispublic = params[3];
            String deviceid = params[4];
            String shopid = params[5];
            JSONParser jsonParser;
            jsonParser = new JSONParser();
            String success;
            String url="http://www.gpl4you.com/mvb/index.php/shops/addNote?";
            String newUrl = url + "name="
                    + name + "&title="
                    + title + "&content="
                    + content+ "&ispublic="
                    + ispublic+ "&device_id="
                    + deviceid+ "&shop_id="
                    + shopid;

            BasicNameValuePair namev = new BasicNameValuePair("name", name);
            BasicNameValuePair titlev = new BasicNameValuePair("title", title);
            BasicNameValuePair contentv = new BasicNameValuePair("content", content);
            BasicNameValuePair ispublicv = new BasicNameValuePair("ispublic", ispublic);
            BasicNameValuePair deviceidv = new BasicNameValuePair("device_id", deviceid);

            BasicNameValuePair shopidv = new BasicNameValuePair("shop_id", shopid);

            // We add the content that we want to pass with the POST request to as name-value pairs
            //Now we put those sending details to an ArrayList with type safe of NameValuePair
            List<NameValuePair> nameValuePairList = new ArrayList<NameValuePair>();
            nameValuePairList.add(namev);
            nameValuePairList.add(titlev);
            nameValuePairList.add(contentv);
            nameValuePairList.add(ispublicv);
            nameValuePairList.add(deviceidv);
            nameValuePairList.add(shopidv);
            Log.e("url",newUrl);
            JSONObject json = jsonParser.makeHttpRequest(newUrl, "GET",nameValuePairList);

            try {
                /*success = json.getString("msg");
                Log.v("SUCCESS:: ", success);
                if (success.trim().toString().equalsIgnoreCase("SUCCESS")) {
                    String loggedUsername = json.getString("USER_NAME");
                    //bundle.putString("loggedUser", loggedUsername);
                    Log.e("Logged User::", loggedUsername);

                }*/
            } catch (Exception e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }

            return null;

        }

        /*
         * (non-Javadoc)
         *
         * @see android.os.AsyncTask#onPostExecute(java.lang.Object)
         */
        protected void onPostExecute(String list) {
            pd.setMessage("Successfully Submitted!!");
            pd.dismiss();
            Toast.makeText(appContext, "Note Successfully Added", Toast.LENGTH_SHORT).show();
            myDialog.dismiss();


        }
    }

    private class FavShopTask extends AsyncTask<String, Void, String> {

        /** The resp. */
        String shopId;

        /*
         * (non-Javadoc)
         *
         * @see android.os.AsyncTask#onPreExecute()
         */
        protected void onPreExecute() {

            pd.setMessage("Submitting...");
            pd.setCancelable(true);
            pd.show();

        }

        /*
         * (non-Javadoc)
         *
         * @see android.os.AsyncTask#doInBackground(Params[])
         */
        protected String doInBackground(final String... params) {

            String shop_id = params[1];
            String deviceid = params[0];
            this.shopId=shop_id;
            JSONParser jsonParser;
            jsonParser = new JSONParser();
            String success;
            String url="http://www.gpl4you.com/mvb/index.php/shops/addFavShop?";
            String newUrl = url + "shop_id="
                    + shop_id + "&device_id="
                    + deviceid;
            Log.e("Favshopurl",newUrl);

            BasicNameValuePair namev = new BasicNameValuePair("shop_id", shop_id);
            BasicNameValuePair deviceidv = new BasicNameValuePair("device_id", deviceid);

            // We add the content that we want to pass with the POST request to as name-value pairs
            //Now we put those sending details to an ArrayList with type safe of NameValuePair
            List<NameValuePair> nameValuePairList = new ArrayList<NameValuePair>();
            nameValuePairList.add(namev);
            nameValuePairList.add(deviceidv);

            JSONObject json = jsonParser.makeHttpRequest(newUrl, "GET",nameValuePairList);

            try {
                /*success = json.getString("msg");
                Log.v("SUCCESS:: ", success);
                if (success.trim().toString().equalsIgnoreCase("SUCCESS")) {
                    String loggedUsername = json.getString("USER_NAME");
                    //bundle.putString("loggedUser", loggedUsername);
                    Log.e("Logged User::", loggedUsername);

                }*/
            } catch (Exception e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }

            return null;

        }

        /*
         * (non-Javadoc)
         *
         * @see android.os.AsyncTask#onPostExecute(java.lang.Object)
         */
        protected void onPostExecute(String list) {
            pd.setMessage("Successfully Added!!");
            favShopList.add(this.shopId);
            pd.dismiss();
            Toast.makeText(appContext, "Done !!", Toast.LENGTH_SHORT).show();
            //Toast.makeText(appContext, "Shop Successfully Added", Toast.LENGTH_SHORT).show();

        }
    }
}
