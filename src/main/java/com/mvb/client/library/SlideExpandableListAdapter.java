package com.mvb.client.library;

import android.view.View;
import android.view.animation.Animation;
import android.widget.ListAdapter;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.mvb.client.R;
import com.mvb.client.Shop;

import java.util.ArrayList;
/**
 * ListAdapter that adds sliding functionality to a list.
 * Uses R.id.expandalbe_toggle_button and R.id.expandable id's if no
 * ids are given in the contructor.
 *
 * @author tjerk
 * @date 6/13/12 8:04 AM
 */
public class SlideExpandableListAdapter extends AbstractSlideExpandableListAdapter {
	private int toggle_button_id;
	private int expandable_view_id;
	public ArrayList<Marker> markers;
	public  GoogleMap map;

	public SlideExpandableListAdapter(ListAdapter wrapped, int toggle_button_id, int expandable_view_id) {
		super(wrapped);
		this.toggle_button_id = toggle_button_id;
		this.expandable_view_id = expandable_view_id;
	}

	public  SlideExpandableListAdapter(ListAdapter wrapped, ArrayList<Marker> markers, GoogleMap map) {
		this(wrapped, R.id.expandable_toggle_button, R.id.expandable);
		this.markers=markers;
		this.map=map;
	}
	public SlideExpandableListAdapter(ListAdapter wrapped) {
		this(wrapped, R.id.expandable_toggle_button, R.id.expandable);
	}


	@Override
	public View getExpandToggleButton(View parent) {
		return parent.findViewById(toggle_button_id);
	}

	@Override
	public View getExpandableView(View parent) {
		return parent.findViewById(expandable_view_id);
	}

	@Override
	public void enableFor(final View button, final View target, final int position) {
		if(target == lastOpen && position!=lastOpenPosition) {
			// lastOpen is recycled, so its reference is false
			lastOpen = null;
		}
		if(position == lastOpenPosition) {
			// re reference to the last view
			// so when can animate it when collapsed
			lastOpen = target;
		}
		int height = viewHeights.get(position, -1);
		if(height == -1) {
			viewHeights.put(position, target.getMeasuredHeight());
			updateExpandable(target,position);
		} else {
			updateExpandable(target, position);
		}

		button.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(final View view) {
/*				LinearLayout layout = (LinearLayout)view.findViewById(R.id.list_item_select);
				layout.setBackgroundColor(Color.parseColor("#8AC487"));*/
				markers.get(position).showInfoWindow();
				/*TextView shop = (TextView)view.findViewById(R.id.shopName);
				MainActivity.shopNameClicked=shop.getText().toString();
				ActionSlideExpandableListView listView;*/
				map.animateCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(((Shop)wrapped.getItem(position)).getLat(),((Shop)wrapped.getItem(position)).getLon()),15));
				Animation a = target.getAnimation();

				if (a != null && a.hasStarted() && !a.hasEnded()) {

					a.setAnimationListener(new Animation.AnimationListener() {
						@Override
						public void onAnimationStart(Animation animation) {
						}

						@Override
						public void onAnimationEnd(Animation animation) {
							view.performClick();
						}

						@Override
						public void onAnimationRepeat(Animation animation) {
						}
					});

				} else {

					target.setAnimation(null);

					int type = target.getVisibility() == View.VISIBLE
							? ExpandCollapseAnimation.COLLAPSE
							: ExpandCollapseAnimation.EXPAND;

					// remember the state
					if (type == ExpandCollapseAnimation.EXPAND) {
						openItems.set(position, true);
					} else {
						openItems.set(position, false);
					}
					// check if we need to collapse a different view
					if (type == ExpandCollapseAnimation.EXPAND) {
						if (lastOpenPosition != -1 && lastOpenPosition != position) {
							if (lastOpen != null) {
								animateView(lastOpen, ExpandCollapseAnimation.COLLAPSE);
								notifiyExpandCollapseListener(
										ExpandCollapseAnimation.COLLAPSE,
										lastOpen, lastOpenPosition);
							}
							openItems.set(lastOpenPosition, false);
						}
						lastOpen = target;
						lastOpenPosition = position;
					} else if (lastOpenPosition == position) {
						lastOpenPosition = -1;
					}
					animateView(target, type);
					notifiyExpandCollapseListener(type, target, position);
				}
			}
		});
	}


	public void clickMarker(   View target,  int position) {
		int type = target.getVisibility() == View.VISIBLE
				? ExpandCollapseAnimation.COLLAPSE
				: ExpandCollapseAnimation.EXPAND;

		// remember the state
		if (type == ExpandCollapseAnimation.EXPAND) {
			openItems.set(position, true);
		} else {
			openItems.set(position, false);
		}
		// check if we need to collapse a different view
		if (type == ExpandCollapseAnimation.EXPAND) {
			if (lastOpenPosition != -1 && lastOpenPosition != position) {
				if (lastOpen != null) {
					animateView(lastOpen, ExpandCollapseAnimation.COLLAPSE);
					notifiyExpandCollapseListener(
							ExpandCollapseAnimation.COLLAPSE,
							lastOpen, lastOpenPosition);
				}
				openItems.set(lastOpenPosition, false);
			}
			lastOpen = target;
			lastOpenPosition = position;
		} else if (lastOpenPosition == position) {
			lastOpenPosition = -1;
		}
		animateView(target, type);
		notifiyExpandCollapseListener(type, target, position);
	}

}
